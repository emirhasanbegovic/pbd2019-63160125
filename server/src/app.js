var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);
var MongoClient = require('mongodb').MongoClient;

var allUsers = {};

var usersTypingWalkers = {};
var usersTypingCyclers = {};
var usersTypingRunners = {};

app.get('/', function(req, res) {
	res.send('Chat Server is running on port 2000')
});


io.on('connection', function(socket) {

	console.log('user connected')


	socket.on('join', function(user) {

		console.log("-------JOIN-----");

		var obj = JSON.parse(user);
		allUsers[socket.id] = {
			"socket": socket,
			"user": obj
		};



		MongoClient.connect("mongodb://localhost:27017/runsup", function(err, client) {
			if (err) throw err;
			console.log("successfully conected to database imback!");
			var db = client.db('runsup');

			db.collection(obj.groupName).find({}).toArray(function(err, result) {
				if (err) throw err;
				var allMessages = {
					userName: obj.userName,
					data: result
				};
				//io.emit('newUserJoined', JSON.stringify(allMessages));
				socket.emit('newUserJoined', JSON.stringify(allMessages));
			});
		});
	});

	socket.on('typing', function(json) {

		var obj = JSON.parse(json);
		var groupName = obj.groupName;

		if (groupName == "runners") {
			usersTypingRunners[obj.userName] = true;
		} else if (groupName == "cyclers") {
			usersTypingCyclers[obj.userName] = true;
		} else if (groupName == "walkers") {
			usersTypingWalkers[obj.userName] = true;
		}



		for (var key in allUsers) {
			if (allUsers[key].user.groupName == groupName && key != socket.id) {
				allUsers[key].socket.emit('someoneistyping', groupName);
			}
		}
	});

	socket.on('disconnect', function(user) {

		console.log("------ DISCONNECT ----------");


		delete allUsers[socket.id]
		socket.emit("userLeft", ' user has left')

	});

	socket.on('newMessage', function(message) {
		var objPrev = JSON.parse(message);
		var b = objPrev.content.length == 0;
		console.log("++++ = " + b);
		if (objPrev.groupName == "runners") {
			delete usersTypingRunners[objPrev.userName];
		} else if (objPrev.groupName === "cyclers") {
			delete usersTypingCyclers[objPrev.userName];
		} else if (objPrev.groupName == "walkers") {
			delete usersTypingWalkers[objPrev.userName];
		}


		if (isEmpty(usersTypingCyclers)) {
			stopTyping("cyclers", socket.id);
		}
		if (isEmpty(usersTypingRunners)) {
			stopTyping("runners", socket.id);
		}
		if (isEmpty(usersTypingWalkers)) {
			stopTyping("walkers", socket.id);
		}

		if (objPrev.content.length > 0) {
			MongoClient.connect("mongodb://localhost:27017/runsup", function(err, client) {

				if (err) throw err;

				console.log("successfully conected to database!");

				var obj = JSON.parse(message);

				var db = client.db('runsup');

				var documentToInsert = {
					userName: obj.userName,
					content: obj.content,
					time: obj.time,
					img: obj.img,
					groupName: obj.groupName,
					type: obj.type
				};

				db.collection(obj.groupName).insertOne(documentToInsert, function(err, result) {
					if (err) throw err;

					io.emit('newmessage', JSON.stringify(documentToInsert));

				});

			});
		}

	});

	function stopTyping(g, socketID) {
		for (var key in allUsers) {
			if (allUsers[key].user.groupName == g) {
				allUsers[key].socket.emit('stoptyping', g);
			}
		}
	}
});

function isEmpty(obj) {
	for (var key in obj) {
		if (obj.hasOwnProperty(key))
			return false;
	}
	return true;
}


server.listen(2000, function() {

	console.log('Node app is running on port 2000')

});