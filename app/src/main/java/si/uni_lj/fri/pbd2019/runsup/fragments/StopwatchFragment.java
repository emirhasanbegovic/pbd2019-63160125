package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.ActiveWorkoutMapActivity;
import si.uni_lj.fri.pbd2019.runsup.PossibleActions;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;

public class StopwatchFragment extends Fragment {


    private TextView durationTextView;
    private TextView distanceTextView;
    private TextView caloriesTextView;
    private TextView paceTextView;
    private int serviceState;
    private ArrayList<LatLng> arrayListCurrentWorkout;
    private SharedPreferences sharedPreferences;


    public class MyReceiver extends BroadcastReceiver {
        private long duration;
        private double distance = 0;
        private double calories = 0;
        private double pace = 0;
        private int updateRouteCounter = 0;
        private Long currentWorkoutID = (long)0;

        private ArrayList<ArrayList<LatLng>> allWorkoutArrayList;

        public MyReceiver(){
            arrayListCurrentWorkout = new ArrayList<LatLng>();
            //TODO ????????????  allWorkoutArrayList = new ArrayList<ArrayList<LatLng>>();
        }

        @Override
        public void onReceive(Context context, Intent intent) {


            duration = intent.getLongExtra("duration", 0);
            durationTextView.setText(MainHelper.formatDuration(duration));
            updateRouteCounter++;

            currentWorkoutID = intent.getLongExtra("workoutId", 0);

            if(updateRouteCounter == 15){ //TODO 15 SEK REDRAW?????
                updateRouteCounter = 0;
            }

            String unit = sharedPreferences.getString("unit", "0");
            boolean km_unit = unit.equals("0");

            distance = intent.getDoubleExtra("distance", 0);
            pace = intent.getDoubleExtra("pace",0);

            if(!km_unit){
                distanceTextView.setText(MainHelper.metersToMiles(distance));
                paceTextView.setText(MainHelper.minPerKmToMinPerMiles(pace));
            }else {
                distanceTextView.setText(MainHelper.formatDistance(distance));
                paceTextView.setText(MainHelper.formatPace(pace));
            }

            calories = intent.getDoubleExtra("calories", 0);
            caloriesTextView.setText(MainHelper.formatCalories(calories));

            serviceState = intent.getIntExtra("state", 0);

            double lat = intent.getDoubleExtra("currentLocationLat", -1);
            double lon = intent.getDoubleExtra("currentLocationLon", -1);

            if(lat != -1){
                LatLng latLng = new LatLng(lat, lon);
                arrayListCurrentWorkout.add(latLng);
            }

            /*ArrayList<Location> mylist = (ArrayList<Location>)intent.getSerializableExtra("positionList");

            if(mylist != null){
                if(mylist.size()>0){
                    Location location = mylist.get(0);
                    currentLocations.add(location);
                }

            }

            if(serviceState == 2){
                arrayListMain.add(currentLocations);
                currentLocations = new ArrayList<>();
            }*/
        }

        public Long getCurrentWorkoutID(){
            return currentWorkoutID;
        }

        public long getDuration() {
            return duration;
        }

        public double getDistance() {
            return distance;
        }

        public double getCalories() {
            return calories;
        }

        public double getPace() {
            return pace;
        }

        public int getServiceState(){
            return serviceState;
        }
    }


    private MyReceiver myReceiver;
    private Intent trackerServiceIntent;

    private int locationRequestCode = 1000;
    private int selectedType = 0;

    private ArrayList<ArrayList<Location>> mainArrayListLocations;

    private Button startButton;
    private Button endWorkoutButton;
    private Button activitySelectionButton;
    private Button openMapButton;
    private TextView textview_stopwatch_distanceunit;
    private TextView textview_stopwatch_unitpace;
    private DatabaseHelper dbHelper;
    private Long currentWorkoutID;

    public static StopwatchFragment newInstance() {
        StopwatchFragment fragment = new StopwatchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stopwatch, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if(sharedPreferences.getBoolean("gps", true)){
            showPermissionDialog();
        }

    }

    private Workout getLastWorkout(){
        dbHelper = getHelper();
        Dao<Workout, Long> workoutDao = null;

        try{
            workoutDao = dbHelper.workoutDao();
            List<Workout> workoutList = workoutDao.queryBuilder().orderBy("id", false).limit(1l).query();
            if(workoutList.size()>0){
                return workoutList.get(0);
            }
            return null;
        }catch (SQLException e){
            e.printStackTrace();
        }

        return null;
    }

    private void syncService(){
        Workout lastWorkout = getLastWorkout();
        if(lastWorkout != null){
            if(lastWorkout.getStatus() == 0 || lastWorkout.getStatus() == 2){ //unknown || paused
                activitySelectionButton.setText(getStateString(lastWorkout.getSportActivity()));
                currentWorkoutID = lastWorkout.getId();
                startButton.setText(R.string.stopwatch_continue);
                endWorkoutButton.setVisibility(View.VISIBLE);
                trackerServiceIntent = new Intent(getActivity(), TrackerService.class);
                trackerServiceIntent.setAction(PossibleActions.WORKOUT_SYNC);
                trackerServiceIntent.putExtra("workoutId", lastWorkout.getId());
                myRecieverRegistration();
                getActivity().startService(trackerServiceIntent);
            }
        }
    }

    private String getStateString(int state){
        switch(state){
            case 0: return "Running";
            case 1: return "Walking";
            case 2: return "Cycling";
        }
        return "fuck do i know";
    }

    @Override
    public void onResume() {
        super.onResume();

        String unit = sharedPreferences.getString("unit", "0");
        boolean km_unit = unit.equals("0");

        if(myReceiver == null){
            distanceTextView.setText("0.00");
            paceTextView.setText("0.00");
        }else{
            if(!km_unit){
                distanceTextView.setText(MainHelper.metersToMiles(myReceiver.getDistance()));
                paceTextView.setText(MainHelper.minPerKmToMinPerMiles(myReceiver.getPace()));
            }else{
                distanceTextView.setText(MainHelper.twoDecimals(myReceiver.getDistance()/1000));
                paceTextView.setText(MainHelper.formatPace(myReceiver.getPace()));
            }

        }

        if(!km_unit){
            textview_stopwatch_distanceunit.setText(R.string.all_labeldistanceunitmiles);
            String paceUnitMiles = getResources().getString(R.string.all_min)+"/"+getResources().getString(R.string.all_labeldistanceunitmiles);
            textview_stopwatch_unitpace.setText(paceUnitMiles);
        }else{
            String paceUnitKm= getResources().getString(R.string.all_min)+"/"+getResources().getString(R.string.all_labeldistanceunitkilometers);
            textview_stopwatch_distanceunit.setText(getResources().getString(R.string.all_labeldistanceunitkilometers));
            textview_stopwatch_unitpace.setText(paceUnitKm);
        }

        if(trackerServiceIntent == null){
            trackerServiceIntent = new Intent(getActivity(), TrackerService.class);
            getActivity().startService(trackerServiceIntent);
            return;
        }

        if(myReceiver == null){
            myRecieverRegistration();
            trackerServiceIntent = new Intent(getActivity(), TrackerService.class);
            getActivity().startService(trackerServiceIntent);
        }else {
            if(myReceiver.getServiceState() == 3) {
                myRecieverRegistration();
                trackerServiceIntent = new Intent(getActivity(), TrackerService.class);
                getActivity().startService(trackerServiceIntent);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if(myReceiver != null && myReceiver.getServiceState() == 3) {
            try {
                getActivity().unregisterReceiver(myReceiver);
            } catch (Exception e) {
                Log.e("RECIEVER ERROR:", e.toString());
            }

            if (trackerServiceIntent != null) {
                try{
                    myRecieverRegistration();
                    trackerServiceIntent = new Intent(getActivity(), TrackerService.class);
                    getActivity().stopService(trackerServiceIntent);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (myReceiver != null)
                getActivity().unregisterReceiver(myReceiver);
            if (trackerServiceIntent != null)
                   getActivity().stopService(trackerServiceIntent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        durationTextView = view.findViewById(R.id.textview_stopwatch_duration);
        distanceTextView = view.findViewById(R.id.textview_stopwatch_distance);
        caloriesTextView = view.findViewById(R.id.textview_stopwatch_calories);
        paceTextView = view.findViewById(R.id.textview_stopwatch_pace);
        mainArrayListLocations = new ArrayList<>();

        textview_stopwatch_distanceunit = view.findViewById(R.id.textview_stopwatch_distanceunit);
        textview_stopwatch_unitpace = view.findViewById(R.id.textview_stopwatch_unitpace);

        startButton = view.findViewById(R.id.button_stopwatch_start);
        endWorkoutButton = view.findViewById(R.id.button_stopwatch_endworkout);
        activitySelectionButton = view.findViewById(R.id.button_stopwatch_selectsport);
        openMapButton = view.findViewById(R.id.button_stopwatch_activeworkout);

        openMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActiveWorkoutMapActivity.class);

                if(arrayListCurrentWorkout!= null && arrayListCurrentWorkout.size()>0){
                    LatLng location = arrayListCurrentWorkout.get(arrayListCurrentWorkout.size()-1);
                    intent.putExtra("initLat", location.latitude);
                    intent.putExtra("initLon", location.longitude);
                    intent.putExtra("locs", arrayListCurrentWorkout);
                }
                startActivity(intent);
            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (startButton.getText().toString().equals(getString(R.string.stopwatch_start))) { //GUMB START
                    startButton.setText(R.string.stopwatch_stop);
                    myRecieverRegistration();
                    startTrackerService();
                } else if (startButton.getText().toString().equals(getString(R.string.stopwatch_stop))) { // GUMB STOP
                    startButton.setText(R.string.stopwatch_continue);
                    endWorkoutButton.setVisibility(View.VISIBLE);
                    pauseTrackerService();
                } else if (startButton.getText().toString().equals(getString(R.string.stopwatch_continue))) { // GUMB CONTINUE
                    startButton.setText(R.string.stopwatch_stop);
                    endWorkoutButton.setVisibility(View.GONE);
                    continueTrackerService();
                }

            }
        });


        endWorkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("End Actual Workout?");
                alertDialog.setMessage("Do you really want to end workout and reset counters ?");
                alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        stopTrackerService();
                        Intent intentDetailsWorkout = new Intent(getActivity(), WorkoutDetailActivity.class);
                        if(myReceiver != null){
                            intentDetailsWorkout.putExtra("duration", myReceiver.getDuration());
                            intentDetailsWorkout.putExtra("distance", myReceiver.getDistance());
                            intentDetailsWorkout.putExtra("calories", myReceiver.getCalories());
                            intentDetailsWorkout.putExtra("pace", myReceiver.getPace());
                            intentDetailsWorkout.putExtra("sportActivity", selectedType);
                            intentDetailsWorkout.putExtra("finalPositionList", mainArrayListLocations);
                            intentDetailsWorkout.putExtra("workoutId", myReceiver.getCurrentWorkoutID());
                        }
                        startActivity(intentDetailsWorkout);
                        endWorkoutButton.setVisibility(View.GONE);
                        startButton.setText(R.string.stopwatch_start);
                    }
                });
                alertDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


                alertDialog.show();

            }
        });

        activitySelectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("select your activity");

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_singlechoice);
                arrayAdapter.add("RUNNING");
                arrayAdapter.add("WALKING");
                arrayAdapter.add("CYCLING");

                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedType = which;
                        trackerServiceIntent = new Intent(getActivity(), TrackerService.class);
                        trackerServiceIntent.setAction(PossibleActions.UPDATE_SPORT_ACTIVITY);
                        trackerServiceIntent.putExtra("activity", selectedType);
                        activitySelectionButton.setText(arrayAdapter.getItem(which));
                        getActivity().startService(trackerServiceIntent);
                    }
                });

                builder.setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.show();


            }
        });

        syncService();

    }

    private void showPermissionDialog() {
        if (!TrackerService.checkPermission(getActivity())) {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    locationRequestCode);
        }
    }

    private void myRecieverRegistration(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(PossibleActions.TICK);
        myReceiver = new MyReceiver();
        getActivity().registerReceiver(myReceiver, filter);
    }

    private void startTrackerService() {
        trackerServiceIntent = new Intent(getActivity(), TrackerService.class);
        trackerServiceIntent.setAction(PossibleActions.COMMAND_START);
        trackerServiceIntent.putExtra("activity", selectedType);
        getActivity().startService(trackerServiceIntent);
    }

    private void pauseTrackerService() {
        trackerServiceIntent = new Intent(getActivity(), TrackerService.class);
        trackerServiceIntent.setAction(PossibleActions.COMMAND_PAUSE);
        getActivity().startService(trackerServiceIntent);
    }

    private void continueTrackerService() {
        trackerServiceIntent = new Intent(getActivity(), TrackerService.class);
        trackerServiceIntent.setAction(PossibleActions.COMMAND_CONTINUE);
        getActivity().startService(trackerServiceIntent);
    }

    private void stopTrackerService() {
        Intent stateStopIntent = new Intent();
        stateStopIntent.setAction(PossibleActions.TICK);
        stateStopIntent.putExtra("state", 3);
        getActivity().sendBroadcast(stateStopIntent);
        trackerServiceIntent = new Intent(getActivity(), TrackerService.class);
        trackerServiceIntent.setAction(PossibleActions.COMMAND_STOP);
        getActivity().startService(trackerServiceIntent);
    }


    private DatabaseHelper getHelper(){
        if(dbHelper == null){
            return OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        }
        return dbHelper;
    }
}
