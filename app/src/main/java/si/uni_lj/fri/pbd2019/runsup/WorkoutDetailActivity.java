package si.uni_lj.fri.pbd2019.runsup;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class WorkoutDetailActivity extends AppCompatActivity {

    private TextView textViewDuration;
    private TextView textViewCalories;
    private TextView textViewDistance;
    private TextView texViewAvgPace;
    private TextView textViewDate;
    private TextView textViewState;
    private TextView textViewTitle;

    private Button fbButton;
    private Button emailButton;
    private Button gPlusButton;
    private Button twitterButton;

    private String dist;
    private String dur;
    private String workoutType;

    private DatabaseHelper dbHelper;
    private SharedPreferences sharedPreferences;
    private GoogleMap mMap;

    private long currentWorkoutID;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_detail);


        setupActionBar();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);


/*        showMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WorkoutDetailActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });*/

        currentWorkoutID = getIntent().getLongExtra("workoutId", 0);

        fbButton = findViewById(R.id.button_workoutdetail_fbsharebtn);
        fbButton.setOnClickListener(listener);
        emailButton = findViewById(R.id.button_workoutdetail_emailshare);
        emailButton.setOnClickListener(listener1);
        gPlusButton = findViewById(R.id.button_workoutdetail_gplusshare);
        gPlusButton.setOnClickListener(listener);
        twitterButton = findViewById(R.id.button_workoutdetail_twittershare);
        twitterButton.setOnClickListener(listener);

        //workoutType = getStateString(getIntent().getIntExtra("sportActivity", 0));
        textViewTitle = findViewById(R.id.textview_workoutdetail_workouttitle);
        //textViewTitle.setText(workoutType);

        textViewTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialogBuilder();
            }
        });

        textViewDate = findViewById(R.id.textview_workoutdetail_activitydate);
        //textViewDate.setText(SimpleDateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime()));

        textViewDuration = findViewById(R.id.textview_workoutdetail_valueduration);
        //dur = MainHelper.formatDuration(getIntent().getLongExtra("duration", 0));
        //textViewDuration.setText(dur);

        textViewDistance = findViewById(R.id.textview_workoutdetail_valuedistance);
        //double vrednostDistance = getIntent().getDoubleExtra("distance",0);
        //d("newdist", "prva prebrana vrednost je "+vrednostDistance);
        //dist = MainHelper.formatDistance(vrednostDistance);
        //Log.d("newdist", "m -> km :  "+dist);
        //textViewDistance.setText(dist +" km");

        textViewCalories = findViewById(R.id.textview_workoutdetail_valuecalories);
        //textViewCalories.setText(MainHelper.formatCalories(getIntent().getDoubleExtra("calories", 0))+ " kcal");

        textViewState = findViewById(R.id.textview_workoutdetail_sportactivity);

       // textViewState.setText(workoutType);

        texViewAvgPace = findViewById(R.id.textview_workoutdetail_valueavgpace);
        double newDist = getIntent().getDoubleExtra("distance",0) / 1000.0;
        //Log.d("newdist", "newdist je "+newDist + "karkol sm prej dobu pa je "+dist);


        double t = getIntent().getLongExtra("duration", 0);
        if(newDist == 0){
        //    texViewAvgPace.setText("0.00"+" min.km");
        }else {
            //Log.d("newdist", "cs k sm ga dobu pa je "+t);
            t = t / 60;
            //Log.d("newdist", "s -> min "+t);
            double avgPace = t / newDist;

            //Log.d("newdist", "ko delim vrednosti "+t+" z vrednostjo "+newDist + "dobim rezultat = "+avgPace);
          //  texViewAvgPace.setText(MainHelper.formatPace(avgPace) + " min.km");
        }


        updateUI(findWorkout());
        mapFragmentPreview();
    }

    private void setDialogBuilder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("EDIT YOUR WORKOUT NAME");

        // Set up the input
        final EditText input = new EditText(this);

        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String userInput = input.getText().toString();
                if(userInput.length()>0){
                    textViewTitle.setText(userInput);
                    updateWorkoutTitle(findWorkout(), userInput);
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


    private Workout findWorkout(){
        dbHelper = getHelper();
        Dao<Workout, Long> workoutDao = null;

        try{
            workoutDao = dbHelper.workoutDao();
            return workoutDao.queryBuilder().where().eq("id", currentWorkoutID).query().get(0);
        }catch (SQLException e){
            e.printStackTrace();
        }

        return null;
    }


    private void updateUI(Workout workout){
        if(workout!= null){

            dist = outputDistanceWithUnit(workout.getDistance());
            dur = MainHelper.formatDuration(workout.getDuration());
            workoutType = getStateString(workout.getSportActivity());

            textViewTitle.setText(workout.getTitle());
            textViewState.setText(getStateString(workout.getSportActivity()));

            if(workout.getCreated()==null){
                textViewDate.setText(SimpleDateFormat.getDateTimeInstance().format(new Date()));
            }else{
                textViewDate.setText(SimpleDateFormat.getDateTimeInstance().format(workout.getCreated()));
            }

            textViewDuration.setText(MainHelper.formatDuration2(workout.getDuration()));
            String caloriesOutput = MainHelper.formatCalories(workout.getTotalCalories())+" "+getResources().getString(R.string.all_labelcaloriesunit);
            textViewCalories.setText(caloriesOutput);

            textViewDistance.setText(dist);
            texViewAvgPace.setText(outputPaceWithUnit(workout.getPaceAvg()));
        }
    }

    private void sendWorkoutPointsAndStartMaps(){
        Workout currentWorkout = findWorkout();
        Intent intent = new Intent(this, MapsActivity.class);
        ArrayList<String> locationList = new ArrayList<>();
        ForeignCollection<GpsPoint> gpsPoints = currentWorkout.getGpsPoints();
        for(GpsPoint gpsPoint : gpsPoints){
            locationList.add(gpsPoint.getLatitude()+"###"+gpsPoint.getLongitude()+"###"+gpsPoint.getSessionNumber());
        }
        intent.putStringArrayListExtra("locations", locationList);
        startActivity(intent);
    }

    private String outputDistanceWithUnit(double distanceInMeters){
        if(distanceUnitIsKm()){
            return MainHelper.formatDistance(distanceInMeters) + " "+getResources().getString(R.string.all_labeldistanceunitkilometers);
        }
        return MainHelper.metersToMiles(distanceInMeters)+ " "+getResources().getString(R.string.all_labeldistanceunitmiles);
    }

    private String outputPaceWithUnit(double pace){
        if(distanceUnitIsKm()){
            return MainHelper.formatPace(pace) + " "+getResources().getString(R.string.all_min)+"/"+getResources().getString(R.string.all_labeldistanceunitkilometers);
        }
        return MainHelper.minPerKmToMinPerMiles(pace) + " "+getResources().getString(R.string.all_min)+"/"+getResources().getString(R.string.all_labeldistanceunitmiles);
    }

    private boolean distanceUnitIsKm(){
        String unit = sharedPreferences.getString("unit", "0");
        return unit.equals("0");
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showShareDialog("MAP");
        }
    };

    private View.OnClickListener listener1 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showShareDialog("NO_MAP");
        }
    };

    private double toMinKm(double mPerS){
        return  ((1/0.06) * mPerS);
    }

    private String getStateString(int state){
        switch(state){
            case 0: return "Running";
            case 1: return "Walking";
            case 2: return "Cycling";
        }
        return "fuck do i know";
    }

    private void showShareDialog(String map) {
        FragmentManager fm = getSupportFragmentManager();
        String title = "Share your workout";
        String msg = String.format("I was out for %s. I did %s in %s.",workoutType, dist, dur);
        ShareDialogFragment editNameDialogFragment = ShareDialogFragment.newInstance(title, msg, map);
        editNameDialogFragment.show(fm, "fragment_edit_name");
    }

    private DatabaseHelper getHelper(){
        if(dbHelper == null){
            return OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return dbHelper;
    }

    private void mapFragmentPreview(){
        Workout workout = findWorkout();
        if(workout!=null && workout.getGpsPoints()!=null){
            if(workout.getGpsPoints().size()<=1){
                findViewById(R.id.fragment_workoutdetail_map).setVisibility(View.GONE);
            }else{
                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.fragment_workoutdetail_map);
                mapFragment.getMapAsync(onMapReadyCallback);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_delete:
                deleteCurrentWorkout(findWorkout());
                this.finish();
                break;
            case R.id.action_settings:
                Intent intent = new Intent(WorkoutDetailActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteCurrentWorkout(Workout workout){
        if(workout!=null){
            workout.setStatus(Workout.statusDeleted);
            updateWorkout(workout);
        }
    }

    private void updateWorkout(Workout workout){
        dbHelper = getHelper();
        Dao<Workout, Long> workoutDao = null;
        workout.setLastUpdate(new Date());
        try{
            workoutDao = dbHelper.workoutDao();
            workoutDao.update(workout);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void updateWorkoutTitle(Workout workout, String newWorkoutTitle){
        if(workout!=null){
            workout.setTitle(newWorkoutTitle);
            updateWorkout(workout);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_delete).setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    private OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;


            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    Workout w = findWorkout();
                    ForeignCollection<GpsPoint> gpsPoints = w.getGpsPoints();

                    ArrayList<LatLng> route = new ArrayList<>();

                    for(GpsPoint gpsPoint : gpsPoints){
                        route.add(new LatLng(gpsPoint.getLatitude(), gpsPoint.getLongitude()));
                    }

                    mMap.getUiSettings().setScrollGesturesEnabled(false);
                    mMap.getUiSettings().setZoomGesturesEnabled(false);

                    mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            sendWorkoutPointsAndStartMaps();
                        }
                    });

                    if(route.size()>1){
                        for(int i = 0; i < route.size()-1; i++){
                            Polyline line = mMap.addPolyline(new PolylineOptions()
                                    .add(route.get(i), route.get(i+1))
                                    .width(5)
                                    .color(Color.RED));
                        }


                        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                        for(LatLng latLng : route){
                            boundsBuilder.include(latLng);
                        }
                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds( boundsBuilder.build(), 50));
                    }else if(route.size()==1){
                        mMap.addMarker(new MarkerOptions().position(route.get(0)));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(route.get(0),18));
                    }


                }
            });

        }
    };

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
