package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.widget.Toast;

import java.util.List;

public final class SportActivities {

    private static final float MpS_TO_MIpH = 2.23694f;

    static double running[] = { 0, 0, 0, 0, 6.0, 8.3, 9.8, 11.0, 11.8, 12.8, 14.5, 16.0, 19.0, 19.8, 23.0, 0, 0, 0, 0,
            0, 0 };
    static double walking[] = { 0, 2.0, 2.8, 3.1, 3.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    static double cycling[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6.8, 0, 8.0, 0, 10.0, 0, 12.8, 0, 13.6, 0, 15.8 };

    private static final double RUNNING_CONSTANT = 1.535353535;
    private static final double WALKING_CONSTANT = 1.14;
    private static final double CYCLING_CONSTANT = 0.744444444;

    public static final int RUNNING = 0;
    public static final int WALKING = 1;
    public static final int CYCLING = 2;

    /**
     * Returns final calories computed from the data provided (returns value in
     * kcal)
     *
     * @param sportActivity
     *            - sport activity type (0 - running, 1 - walking, 2 - cycling)
     * @param weight
     *            - weight in kg
     * @param speedList
     *            - list of all speed values recorded (unit = m/s)
     * @param timeFillingSpeedListInHours
     *            - time of collecting speed list (duration of sport activity from
     *            first to last speedPoint in speedList)
     * @return
     */
    public static double countCalories(int sportActivity, float weight, List<Float> speedList,
                                       double timeFillingSpeedListInHours) {

        float avg = 0;
        for(int i = 0; i < speedList.size(); i++) {
            avg+=speedList.get(i);
        }

        avg/=speedList.size();

        return getMET(sportActivity, avg ) * weight * timeFillingSpeedListInHours;

    }



    /**
     * Returns MET value for an activity.
     *
     * @param activityType
     *            - sport activity type (0 - running, 1 - walking, 2 - cycling)
     * @param speed
     *            - speed in m/s
     * @return
     */
    public static double getMET(int activityType, Float speed) {
        double v = mpsToMiph(speed);
        int index = (int) Math.ceil(v);

        if (index > 20) {
            return getMet2(activityType, index);
        }
        switch (activityType) {
            case 0:
                if (running[index] == 0) {
                    return getMet2(activityType, v);
                }
                return running[index];
            case 1:
                if (walking[index] == 0) {
                    return getMet2(activityType, v);
                }
                return walking[index];
            case 2:
                if (cycling[index] == 0) {
                    return getMet2(activityType, v);
                }
                return cycling[index];
        }

        return -1;
    }

    private static double getMet2(int activityType, double speed) {
        switch (activityType) {
            case 0:
                return speed * RUNNING_CONSTANT;
            case 1:
                return speed * WALKING_CONSTANT;
            case 2:
                return speed * CYCLING_CONSTANT;
        }
        return -1;
    }

    /* convert m/s to mi/h (multiply with a corresponding constant) */
    public static double mpsToMiph(double n){
        return n * MpS_TO_MIpH;
    }

}
