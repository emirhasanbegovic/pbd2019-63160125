package si.uni_lj.fri.pbd2019.runsup;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MessageAdapter extends ArrayAdapter<MyMessage> {

    private Context context;
    private List<MyMessage> messages;
    private String userName;
    private SharedPreferences sharedPreferences;

    public MessageAdapter(@NonNull Context context, int resource, ArrayList<MyMessage> messages, String userName) {
        super(context, resource);
        this.context = context;
        this.messages = messages;
        this.userName = userName;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public MyMessage getItem(int position) {
        // TODO Auto-generated method stub
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        MyMessage currentMessage = messages.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.adapter_message, null);

        TextView message = view.findViewById(R.id.sender_tv_message);
        ImageView imageViewMessage = view.findViewById(R.id.image_view_message);

        if (!userName.equals(currentMessage.getName())) {
            message.setBackground(context.getDrawable(R.drawable.message2));
        }


        TextView username = view.findViewById(R.id.text_message_name);
        username.setText(currentMessage.getName());

        TextView timeStampText = view.findViewById(R.id.text_message_time);
        TextView timeStampImg = view.findViewById(R.id.image_message_time);

        ImageView imageView = view.findViewById(R.id.image_message_profile);
        Glide.with(context).load(currentMessage.getUserImgURL()).into(imageView);

        final String senderUserName = currentMessage.getName();
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subscribeDialog(senderUserName);
            }
        });

        if (currentMessage.getType().equals("text")) {
            message.setVisibility(View.VISIBLE);
            imageViewMessage.setVisibility(View.GONE);
            timeStampText.setVisibility(View.VISIBLE);
            timeStampImg.setVisibility(View.GONE);
            timeStampText.setText(currentMessage.getTime());
            message.setText(currentMessage.getContent());
        } else {
            message.setVisibility(View.GONE);
            imageViewMessage.setVisibility(View.VISIBLE);
            timeStampText.setVisibility(View.GONE);
            timeStampImg.setVisibility(View.VISIBLE);
            timeStampImg.setText(currentMessage.getTime());
            Glide.with(context).load(Base64.decode(currentMessage.getContent(), Base64.DEFAULT)).crossFade().fitCenter().into(imageViewMessage);
        }

        return view;
    }

    private void subscribeDialog(final String userToSub) {
        if (!userToSub.equals(userName)) {
            new AlertDialog.Builder(context)
                    .setTitle("Tag  " + userToSub + " to sub messages notifications")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if(!userIsSubscribed(userToSub)){
                                Toast.makeText(context, "Already subscribed to this user", Toast.LENGTH_SHORT).show();
                                writeToSharedPrefrences(userToSub);
                            }
                        }
                    })
                    .setNegativeButton(R.string.unsubscibe, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            unsubscribe(userToSub);
                        }
                    }).setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
            }).show();
        }
    }

    private boolean userIsSubscribed(String otherUser){
        String currentSubscriptions = getCurrentSubscriptions();
        Log.d("subscriptions1", currentSubscriptions);
        String arrSubscriptions[] = currentSubscriptions.split(";");
        Log.d("subscriptions1", Arrays.toString(arrSubscriptions));
        for(int i = 0; i < arrSubscriptions.length; i++){
            if(arrSubscriptions[i].equals(otherUser)){
                return true;
            }
        }
        return false;
    }

    private String getCurrentSubscriptions() {
        return sharedPreferences.getString("subscriptions", "");
    }

    private void writeToSharedPrefrences(String userToSub) {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        String currentSubscriptions = getCurrentSubscriptions();

        currentSubscriptions += userToSub + ";";

        Log.d("subscriptions1", "tole pisem not "+currentSubscriptions);
        editor.putString("subscriptions", currentSubscriptions);
        editor.apply();
    }

    private void unsubscribe(String userToSub) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String currentSubscriptions = getCurrentSubscriptions();

        String arrSubscriptions[] = currentSubscriptions.split(";");

        if(arrSubscriptions.length==0){
            return;
        }

        String str = "";
        for(int i = 0; i < arrSubscriptions.length; i++){
            if(arrSubscriptions[i].equals(userToSub)){
               continue;
            }
            if(i == arrSubscriptions.length-1){
                str+=arrSubscriptions[i];
            }else{
                str+=arrSubscriptions[i]+";";
            }
        }

        editor.putString("subscriptions", str);
        editor.apply();
    }

}
