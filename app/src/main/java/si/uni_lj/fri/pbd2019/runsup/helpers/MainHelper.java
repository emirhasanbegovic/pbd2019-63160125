package si.uni_lj.fri.pbd2019.runsup.helpers;

public final class MainHelper {

    private static final float MpS_TO_MIpH = 2.23694f;
    private static final float KM_TO_MI = 0.62137119223734f;
    private static final float MINpKM_TO_MINpMI = 1.609344f;
    private static final float KMH_TO_MPH = 0.621371192f;

    /**
     * return string of time in format HH:MM:SS
     * @param time - in seconds
     */
    public static String formatDuration(long time){
        long SS = time%60;
        long MM = (time / 60) % 60;
        double d = (double)time/(60*60);
        double ost = d%1;
        long HH = (long)(d-ost);
        return String.format("%02d:%02d:%02d",HH, MM, SS);
    }

    public static String formatDuration2(long time){
        long SS = time%60;
        long MM = (time / 60) % 60;
        double d = (double)time/(60*60);
        double ost = d%1;
        long HH = (long)(d-ost);
        return String.format("%d:%02d:%02d",HH, MM, SS);
    }

    /**
     * convert m to km and round to 2 decimal places and return as string
     */
    public static String formatDistance(double n){
        return formatPace(n/1000.0);
    }



    /**
     * round number to 2 decimal places and return as string
     */
    public static String formatPace(double n){
        return String.format("%.2f", n);
        //return Double.toString( Math.round(n * 100.0) / 100.0);
    }

    public static String twoDecimals(double n){
        return String.format("%.2f", n);
        //return Double.toString( Math.round(n * 100.0) / 100.0);
    }

    /**
     * round number to integer
     */
    public static String formatCalories(double n){
        int val = (int) Math.round(n);
        return Integer.toString(val);
    }

    public static String metersToMiles(double meters){
        double metersToKm = kmToMi(meters/1000);
        return formatPace(metersToKm);
    }

    public static String minPerKmToMinPerMiles(double n){
        double minPerMiles = n * 1.609344;
        return twoDecimals(minPerMiles);
    }

    /* convert km to mi (multiply with a corresponding constant) */
    public static double kmToMi(double n){
        return KM_TO_MI * n;
    }

    /* convert m/s to mi/h (multiply with a corresponding constant) */
    public static double mpsToMiph(double n){
        return n * MpS_TO_MIpH;
    }


    /* convert min/km to min/mi (multiply with a corresponding constant) */
    public static double minpkmToMinpmi(double n){
        return n * MINpKM_TO_MINpMI;
    }

    public static double kmphToMiph(double n){
        return n * KMH_TO_MPH;
    }

}
