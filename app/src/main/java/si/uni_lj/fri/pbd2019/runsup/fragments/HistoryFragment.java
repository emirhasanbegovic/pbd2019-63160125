package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.HistoryListAdapter;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.ServerSync;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class HistoryFragment extends Fragment {

    private ArrayAdapter<String> historyArrayAdapter;
    private static final String TAG = "HistoryFragment1";
    public ArrayList<String> workoutList;
    private DatabaseHelper dbHelper;
    private SharedPreferences sharedPreferences;
    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        return inflater.inflate(R.layout.fragment_history, container, false);
    }


    @Override
    public void onResume() {
        super.onResume();
        workoutList.clear();
        ArrayList<String> allWorkouts = workoutOutput(getWorkouts());
        Log.d(TAG, "onResume() ahjhaahhahahahahahaha "+allWorkouts.size());
        if(!userLoggedIn()){
            //Toast.makeText(getActivity(), "user ni log inan onresume!", Toast.LENGTH_SHORT).show();
            getView().findViewById(R.id.textview_history_noHistoryData).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.listview_history_workouts).setVisibility(View.GONE);
            return;
        }
        if(allWorkouts.size()==0 || !userLoggedIn()){
            getView().findViewById(R.id.textview_history_noHistoryData).setVisibility(View.VISIBLE);
        }else{
            getView().findViewById(R.id.textview_history_noHistoryData).setVisibility(View.GONE);
        }
        workoutList.addAll(workoutOutput(getWorkouts()));
        historyArrayAdapter.notifyDataSetChanged();
    }

    public void updateAdapter(){
        workoutList.clear();
        List<Workout> workouts = getWorkouts();
        for(Workout workout : workouts){
            Log.d("updateAdapter", "ELEMENT00 = "+workout.getCreated());
        }

        workoutList =  workoutOutput(getWorkouts());
        if(listView.getAdapter() == null){
            Log.d("updateAdapter", "adapter is not set yet");
        }

        Log.d("updateAdapter", "----------------------------");
        for(int i = 0; i < workoutList.size(); i++){
            Log.d("updateAdapter", "ELEMENT = "+workoutList.get(i));
        }


        getView().findViewById(R.id.textview_history_noHistoryData).setVisibility(View.GONE);
        ((HistoryListAdapter)historyArrayAdapter).refresh(workoutList);

        if(workouts.size()==0){
            getView().findViewById(R.id.textview_history_noHistoryData).setVisibility(View.VISIBLE);
        }

        /*ArrayList<String> allWorkouts = workoutOutput(getWorkouts());
        Log.d("updateAdapter", "ahjhaahhahahahahahaha "+allWorkouts.size());

        Log.d("updateAdapter", "----------------------------");
        for(int i = 0; i < allWorkouts.size(); i++){
            Log.d("updateAdapter", "ELEMENT = "+allWorkouts.get(i));
        }

        Log.d("updateAdapter", "----------------------------");

        if(allWorkouts.size()==0){
            getView().findViewById(R.id.textview_history_noHistoryData).setVisibility(View.VISIBLE);
        }else{
            getView().findViewById(R.id.textview_history_noHistoryData).setVisibility(View.GONE);
        }

        workoutList = allWorkouts;

        Log.d("updateAdapter", "----------------------------");
        for(int i = 0; i < workoutList.size(); i++){
            Log.d("updateAdapter", "ELEMENT2 = "+workoutList.get(i));
        }
        */
        //Log.d("updateAdapter", "----------------------------");
        //historyArrayAdapter.notifyDataSetChanged();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        sharedPreferences =  PreferenceManager.getDefaultSharedPreferences(getActivity());
        super.onCreate(savedInstanceState);
        workoutList = workoutOutput(getWorkouts());
        historyArrayAdapter = new HistoryListAdapter(getActivity(), 0, workoutList);
        setupActionBar();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.d(TAG, "onCreateOptionsMenu()");
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        Log.d(TAG, "onViewCreated()");
        listView = view.findViewById(R.id.listview_history_workouts);
        getActivity().invalidateOptionsMenu();
        workoutList = workoutOutput(getWorkouts());

        swipeRefreshLayout = view.findViewById(R.id.history_fragment_refresh_layout);


        final HistoryFragment historyFragment = this;
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if(!userLoggedIn()){
                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.setEnabled(false);
                    return;
                }
                //Toast.makeText(getActivity(), "lol", Toast.LENGTH_SHORT).show();
                //rest api data refresh
                //adapter.notifyDataSetChanged(); don't forget
                ServerSync serverSync = ServerSync.getServerSyncInstance(getActivity());
                serverSync.setSwipeRefreshLayout(swipeRefreshLayout, historyFragment);
            }
        });


        if(!userLoggedIn()){
            //Toast.makeText(getActivity(), "user ni log inan onview created!", Toast.LENGTH_SHORT).show();
            view.findViewById(R.id.textview_history_noHistoryData).setVisibility(View.VISIBLE);
            view.findViewById(R.id.listview_history_workouts).setVisibility(View.GONE);
            return;
        }

        if(workoutList.size()==0){
            view.findViewById(R.id.textview_history_noHistoryData).setVisibility(View.VISIBLE);
        }

        historyArrayAdapter = new HistoryListAdapter(getActivity(), 0, workoutList);
        listView.setAdapter(historyArrayAdapter);
    }

    private List<Workout> getWorkouts(){
        dbHelper = getHelper();
        Dao<Workout, Long> workoutDao = null;
        try{
            workoutDao = dbHelper.workoutDao();
            return workoutDao.queryBuilder().where().ne("status", Workout.statusDeleted).query();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<String> workoutOutput(List<Workout> workouts){
        ArrayList<String> arrayList = new ArrayList<>();
            if(workouts != null){

                for(int i = 0; i < workouts.size(); i++){
                    Workout w = workouts.get(i);
                    if(w!=null && w.getStatus()==Workout.statusEnded){
                        String dur = MainHelper.formatDuration2(w.getDuration());
                        String dist = outputDistanceWithUnit(w.getDistance());
                        String activityTpye = getStateString(w.getSportActivity());
                        String cal = MainHelper.formatCalories(w.getTotalCalories()) +" "+getResources().getString(R.string.all_labelcaloriesunit);
                        String avg = outputPaceWithUnit(w.getPaceAvg());

                        String s = String.format("%s %s | %s | %s | avg %s", dur, activityTpye, dist, cal, avg);
                        if(w.getCreated()!=null){
                            String outputString = w.getTitle()+"###"+SimpleDateFormat.getDateTimeInstance().format(w.getCreated())+"###"+s+"###"+w.getId();
                            arrayList.add(outputString);
                        }else{
                            String outputString = w.getTitle()+"###"+SimpleDateFormat.getDateTimeInstance().format(new Date())+"###"+s+"###"+w.getId();
                            arrayList.add(outputString);
                        }
                    }
                }
        }
        return arrayList;
    }

    private String getStateString(int state){
        switch(state){
            case 0: return "Running";
            case 1: return "Walking";
            case 2: return "Cycling";
        }
        return "fuck do i know";
    }

    private DatabaseHelper getHelper(){
        if(dbHelper == null){
            return OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        }
        return dbHelper;
    }


    private String outputDistanceWithUnit(double distanceInMeters){
        if(distanceUnitIsKm()){
            return MainHelper.formatDistance(distanceInMeters) + " "+getResources().getString(R.string.all_labeldistanceunitkilometers);
        }
        return MainHelper.metersToMiles(distanceInMeters)+ " "+getResources().getString(R.string.all_labeldistanceunitmiles);
    }

    private String outputPaceWithUnit(double pace){
        if(distanceUnitIsKm()){
            return MainHelper.formatPace(pace) + " "+getResources().getString(R.string.all_min)+"/"+getResources().getString(R.string.all_labeldistanceunitkilometers);
        }
        return MainHelper.minPerKmToMinPerMiles(pace) + " "+getResources().getString(R.string.all_min)+"/"+getResources().getString(R.string.all_labeldistanceunitmiles);
    }

    private boolean distanceUnitIsKm(){
        String unit = sharedPreferences.getString("unit", "0");
        return unit.equals("0");
    }

    private boolean userLoggedIn(){
        return sharedPreferences.getBoolean("userSignedIn", false);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_clear_history).setVisible(true);
        if(userLoggedIn()){
            menu.findItem(R.id.action_sync_with_server).setVisible(true);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_clear_history:
                openDialog();
                break;
            case R.id.action_sync_with_server:
                ServerSync serverSync = ServerSync.getServerSyncInstance(getActivity());
                serverSync.syncUserData(null, this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure that you want to clear your history data?");
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clearHistory();
                workoutList.clear();
                historyArrayAdapter.notifyDataSetChanged();
                getView().findViewById(R.id.textview_history_noHistoryData).setVisibility(View.VISIBLE);
            }
        });

        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    private void clearHistory(){
        dbHelper = getHelper();
        Dao<Workout, Long> workoutDao = null;
        try{
            workoutDao = dbHelper.workoutDao();
            UpdateBuilder<Workout, Long> updateBuilder = workoutDao.updateBuilder();
            updateBuilder.updateColumnValue("status", Workout.statusDeleted);
            updateBuilder.where().ne("status", Workout.statusDeleted);
            updateBuilder.update();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public ArrayAdapter<String> getHistoryArrayAdapter(){
        return this.historyArrayAdapter;
    }
}
