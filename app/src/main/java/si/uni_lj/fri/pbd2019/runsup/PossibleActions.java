package si.uni_lj.fri.pbd2019.runsup;

public class PossibleActions {

    public static final String COMMAND_START = "si.uni_lj.fri.pbd2019.runsup.COMMAND_START";
    public static final String COMMAND_CONTINUE = "si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE";
    public static final String COMMAND_PAUSE = "si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE";
    public static final String COMMAND_STOP = "si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP";
    public static final String TICK = "si.uni_lj.fri.pbd2019.runsup.TICK";
    public static final String UPDATE_SPORT_ACTIVITY = "si.uni_lj.fri.pdb2019.runsup.UPDATE_SPORT_ACTIVITY";
    public static final String WORKOUT_SYNC = "si.uni_lj.fri.pbd2019.runsup.WORKOUT_SYNC";
}
