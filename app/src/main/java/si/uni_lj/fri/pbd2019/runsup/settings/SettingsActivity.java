package si.uni_lj.fri.pbd2019.runsup.settings;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;

import si.uni_lj.fri.pbd2019.runsup.R;


public class SettingsActivity extends AppCompatPreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreference()).commit();
    }

    private static Preference.OnPreferenceChangeListener myPrefListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {

            if(preference instanceof ListPreference){
                ListPreference listPreference = (ListPreference)preference;
                int i = listPreference.findIndexOfValue(newValue.toString());
                preference.setSummary( i>=0 ? listPreference.getEntries()[i] : null);
            }else {
                preference.setSummary(newValue.toString());
            }

            return true;

        }
    };

    public static void bindSummary(Preference preference){
        preference.setOnPreferenceChangeListener(myPrefListener);
        myPrefListener.onPreferenceChange(preference, PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                                                .getString(preference.getKey(), ""));
    }

    public static class MyPreference extends PreferenceFragment {

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_main);
            setHasOptionsMenu(true);
            bindSummary(findPreference("unit"));
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            //inflater.inflate(R.menu.main, menu);
            super.onCreateOptionsMenu(menu, inflater);
        }
    }


    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
