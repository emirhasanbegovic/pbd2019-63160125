package si.uni_lj.fri.pbd2019.runsup.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.PossibleActions;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;


public class TrackerService extends Service {

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;

    private ArrayList<Location> locationArrayList;
    private ArrayList<Float> speedArrayList;

    private LocationCallback locationCallback;

    private Handler handler;
    boolean alreadySend = false;

    private long duration = 0;
    private double distance = 0;
    private double calories = 0;
    private long speedDuration = 0;
    private long paceTimer = 0;
    private double pace = 0;
    private int currentState = 3;
    private Location pausedLocation;

    private int workoutDatabseState = 0;
    private int selectedActivity = 0;
    private DatabaseHelper dbHelper;

    private Workout currentWorkout;
    private Long currentWorkoutID;

    private int tenSecondsCounter = 0;

    boolean firstTime = false;
    private Location firstTimeLocation;


    private long sessionNumber = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationArrayList = new ArrayList<>();
        speedArrayList = new ArrayList<>();
        createLocationRequest();
        createLocationCallback();
        handler = new Handler();
    }

    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }


    protected void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(3000);
        locationRequest.setSmallestDisplacement(10);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void createLocationCallback() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                Location location1 = locationResult.getLastLocation();

                if(!firstTime){
                    firstTimeLocation = location1;
                }

                if(locationArrayList.size() > 0) {
                    Location location2 = locationArrayList.get(locationArrayList.size()-1);
                    float newDistance = location1.distanceTo(location2);
                    if (newDistance >= 2) {
                        tenSecondsCounter = 0;
                        float calculatedSpeed = calculateSpeed(location1, location2);
                        speedArrayList.add(calculatedSpeed);

                        //locationArrayList.add(location1);

                        addGpsPoint(location1);
                        updateWorkoutStatus();
                        alreadySend = false;
                        speedDuration = duration;
                        distance+=newDistance;
                        double d = duration;
                        if(speedArrayList.size() >= 2){
                            calories=SportActivities.countCalories(selectedActivity, 80, speedArrayList, d/(60*60));
                        }
                        paceTimer = duration;
                    }
                }else{
                    tenSecondsCounter = 0;
                    alreadySend = false;
                    
                    addGpsPoint(location1);
                    updateWorkoutStatus();
                    speedDuration = 0;
                    paceTimer = 0;
                }
            }
        };
    }

    private float calculateSpeed(Location location1, Location location2){
        if(duration - speedDuration == 0){
            return 0;
        }
        return location1.distanceTo(location2) / (duration - speedDuration);
    }


    @SuppressLint("MissingPermission")
    protected void startLocationUpdates(){
        try{
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
        }catch (Exception e){
            Log.e("TrackerServiceERROR", "could not create fusedLocationProviderClient"+ e.toString());
        }

    }



    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public TrackerService getService() {
            // Return this instance of LocalService so clients can call public methods
            return TrackerService.this;
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try{
            startLocationUpdates();
        }catch (Exception e){

        }

        if(intent != null && intent.getAction() != null){
            if(intent.getAction().equals(PossibleActions.COMMAND_START)){
                workoutDatabseState = Workout.statusUnknown;
                sessionNumber++;
                handler.post(runnable);
                addWorkout();
                addGpsPoint(firstTimeLocation);
            }else if(intent.getAction().equals(PossibleActions.COMMAND_CONTINUE)){
                workoutDatabseState = Workout.statusUnknown;
                updateWorkoutStatus();

                if(locationArrayList.size()>0){
                    Location newLocation = locationArrayList.get(locationArrayList.size()-1);
                    addGpsPoint(newLocation);
                    if(pausedLocation != null){
                        float dist = newLocation.distanceTo(pausedLocation);
                        if(dist<=100){
                            distance+=dist;
                        }
                    }
                }

                handler.post(runnable);
                Intent stateContinueIntent = new Intent();
                stateContinueIntent.setAction(PossibleActions.TICK);
                stateContinueIntent.putExtra("state", 1);
                currentState = 1;
                appendData(stateContinueIntent);
                sendBroadcast(stateContinueIntent);

            }else if(intent.getAction().equals(PossibleActions.COMMAND_PAUSE)){
                workoutDatabseState = Workout.statusPaused;
                sessionNumber++;
                updateWorkoutStatus();
                fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                handler.removeCallbacks(runnable);
                Intent statePausedIntent = new Intent();
                statePausedIntent.setAction(PossibleActions.TICK);
                statePausedIntent.putExtra("state", 2);
                currentState = 2;

                appendData(statePausedIntent);
                sendBroadcast(statePausedIntent);
                if(locationArrayList.size()>0){
                    pausedLocation = locationArrayList.get(locationArrayList.size()-1);
                }
                //locationArrayList = new ArrayList<>(); hmm
            }else if(intent.getAction().equals(PossibleActions.COMMAND_STOP)){
                workoutDatabseState = Workout.statusEnded;
                updateWorkoutStatus();
                handler.removeCallbacks(runnable);
                currentState = 3;
                fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                stopSelf();
            }else if(intent.getAction().equals(PossibleActions.UPDATE_SPORT_ACTIVITY)){
                selectedActivity = intent.getIntExtra("activity", 0);
                updateActivityType();
            }else if(intent.getAction().equals(PossibleActions.WORKOUT_SYNC)){
                currentWorkoutID = intent.getLongExtra("workoutId", 1);
                currentWorkout = getWorkoutWithID();
                syncParams();
            }
        }

        return START_STICKY;

    }

    private void syncParams(){
        speedArrayList = new ArrayList<>();
        duration = currentWorkout.getDuration();
        distance = currentWorkout.getDistance();
        pace = currentWorkout.getPaceAvg();
        calories = currentWorkout.getTotalCalories();
        selectedActivity = currentWorkout.getSportActivity();
        workoutDatabseState = Workout.statusPaused;
        ForeignCollection<GpsPoint> foreignCollectionPoints = currentWorkout.getGpsPoints();

        locationArrayList = new ArrayList<>();

        for(GpsPoint gpsPoint : foreignCollectionPoints){
            speedArrayList.add(gpsPoint.getSpeed());
            sessionNumber =  gpsPoint.getSessionNumber();
            Location loc = new Location("");
            loc.setLatitude(gpsPoint.getLatitude());
            loc.setLongitude(gpsPoint.getLongitude());
            locationArrayList.add(loc);
        }

        sessionNumber++;
        speedDuration = duration;
        firstTime = false;
        firstTimeLocation = null;
        alreadySend = false;
        paceTimer = duration;
        currentState = 2;
        pausedLocation = null;
        tenSecondsCounter = 0;


        Intent intent = new Intent();
        intent.setAction(PossibleActions.TICK);

        intent.putExtra("state", currentState);

        appendData(intent);
        sendBroadcast(intent);

    }



    private Workout getWorkoutWithID(){
        DatabaseHelper dbHelper = getHelper();
        Dao<Workout, Long> workoutDao = null;

        try{
            workoutDao = dbHelper.workoutDao();
            return workoutDao.queryBuilder().where().eq("id", currentWorkoutID).query().get(0);
        }catch (SQLException e){
            e.printStackTrace();
        }

        return null;
    }

    private void addWorkout(){
        if(currentWorkout == null) {
            dbHelper = getHelper();
            Dao<Workout, Long> workoutDao = null;
            Dao<User, Long> userDao = null;
            try {
                workoutDao = dbHelper.workoutDao();
                userDao = dbHelper.userDao();
                long workoutID = workoutDao.countOf() + 1;
                String workoutTitle = "Workout " + workoutID;
                currentWorkout = new Workout(workoutTitle, selectedActivity);
                currentWorkout.setStatus(workoutDatabseState);
                currentWorkout.setDistance(0);
                currentWorkout.setDuration(0);
                currentWorkout.setTotalCalories(0);
                currentWorkout.setPaceAvg(0);
                currentWorkout.setLastUpdate(new Date());
                currentWorkout.setCreated(new Date());
                currentWorkoutID = currentWorkout.getId();
                User currentUser = getUser();
                if (currentUser != null) {
                    currentUser.getWorkouts().add(currentWorkout);
                    userDao.update(currentUser);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateActivityType(){
        if(currentWorkout != null){
            currentWorkout.setSportActivity(selectedActivity);
            updateWorkoutStatus();
        }else{
            addWorkout();
        }
    }

    private void updateWorkoutStatus(){
        if(currentWorkout != null){
            currentWorkout.setStatus(workoutDatabseState);
            if(distance == 0){
                currentWorkout.setPaceAvg(0);
            }else{
                currentWorkout.setPaceAvg(duration/distance);
            }
            currentWorkout.setDistance(distance);
            currentWorkout.setDuration(duration);
            currentWorkout.setTotalCalories(calories);
            currentWorkout.setSportActivity(selectedActivity);
            updateWorkout();
        }
    }

    private void addGpsPoint(Location location){

        if(location!=null){

            float speed = 0;
            if(speedArrayList.size()!=0){
                speed = speedArrayList.get(speedArrayList.size()-1);
            }

            if(!firstTime && currentWorkout!=null) {
                firstTime = true;
            }else if(!firstTime){
                firstTime = true;
            }else{
                if(locationArrayList.size()>0){
                    Location lastLocation = locationArrayList.get(locationArrayList.size()-1);
                    if(lastLocation.distanceTo(location)>=10){
                        locationArrayList.add(location);
                        GpsPoint gpsPoint = new GpsPoint(currentWorkout, sessionNumber, location, duration, speed, pace, calories);
                        gpsPoint.setCreated(new Date());
                        gpsPoint.setLastUpdate(new Date());
                        addToDatabase(gpsPoint);
                    }
                }else{
                    locationArrayList.add(location);
                    GpsPoint gpsPoint = new GpsPoint(currentWorkout, sessionNumber, location, duration, speed, pace, calories);
                    gpsPoint.setCreated(new Date());
                    gpsPoint.setLastUpdate(new Date());
                    addToDatabase(gpsPoint);
                }


            }



        }

    }

    private void addToDatabase(GpsPoint gpsPoint){
        dbHelper = getHelper();
        Dao<GpsPoint, Long> gpsDao = null;
        try{
            gpsDao = dbHelper.gpsPointDao();
            gpsDao.create(gpsPoint);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void updateWorkout(){
        dbHelper = getHelper();
        Dao<Workout, Long> workoutDao = null;
        try{
            Log.d("workoutUPDATE", "workout is updated");
            workoutDao = dbHelper.workoutDao();
            currentWorkout.setLastUpdate(new Date());
            workoutDao.update(currentWorkout);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private User getUser(){
        dbHelper = getHelper();
        Dao<User, Long> userDao = null;
        try {
            userDao = dbHelper.userDao();
            List<User> users =  userDao.queryForAll();
            if(users.size()>0){
                return users.get(0);
            }
            return null;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            duration++;


            tenSecondsCounter++;

            if(tenSecondsCounter > 10){
                currentWorkout.setDistance(distance);
                currentWorkout.setDuration(duration);
                currentWorkout.setTotalCalories(calories);
                if(distance == 0){
                    currentWorkout.setPaceAvg(0);
                }else{
                    currentWorkout.setPaceAvg(duration/distance);
                }

                currentWorkout.setLastUpdate(new Date());
                updateWorkoutStatus();
                tenSecondsCounter = 0;
            }
            Intent intent = new Intent();
            intent.setAction(PossibleActions.TICK);
            intent.putExtra("state", 0);
            currentState = 0;
            /*if(locationArrayList.size() > 0){
                Location lastArrLocation = locationArrayList.get(locationArrayList.size()-1);
                if(currentLocation == null){
                    ArrayList<Location> newArr = new ArrayList<>();
                    newArr.add(lastArrLocation);
                    intent.putExtra("positionList",newArr);
                    currentLocation = new Location("");
                    currentLocation.setLatitude(lastArrLocation.getLatitude());
                    currentLocation.setLongitude(lastArrLocation.getLongitude());
                }else {
                    if(currentLocation.getLongitude() != lastArrLocation.getLongitude() || currentLocation.getLatitude() != lastArrLocation.getLatitude()){
                        ArrayList<Location> newArr = new ArrayList<>();
                        newArr.add(lastArrLocation);
                        intent.putExtra("positionList",newArr);
                        currentLocation.setLatitude(lastArrLocation.getLatitude());
                        currentLocation.setLongitude(lastArrLocation.getLongitude());
                    }
                }
            }*/


            if(duration - paceTimer < 6){
                pace = 0;
                if(speedArrayList.size()>0){
                    pace = toMinKm(speedArrayList.get(speedArrayList.size()-1));
                }
            }else{
                pace = 0;
            }

            appendData(intent);
            sendBroadcast(intent);

            handler.postDelayed(this, 1000);
        }
    };

    private void appendData(Intent intent){
        intent.putExtra("duration", duration);
        intent.putExtra("distance", distance);
        intent.putExtra("calories", calories);
        intent.putExtra("sportActivity", selectedActivity);
        if(locationArrayList.size()>0 && !alreadySend){

            Location lastLocation = locationArrayList.get(locationArrayList.size()-1);
            intent.putExtra("currentLocationLat", lastLocation.getLatitude());
            intent.putExtra("currentLocationLon", lastLocation.getLongitude());
            intent.putExtra("location", lastLocation);
            alreadySend = true;
        }
        intent.putExtra("workoutId", currentWorkout.getId());
        intent.putExtra("pace", pace);
    }


    private float toMinKm(float mPerS){
        return (float) (0.0372822715 * mPerS);
    }

    @Override
    public boolean stopService(Intent name) {
        clearAll();
        return super.stopService(name);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onDestroy() {
        clearAll();
    }

    public void clearAll(){
        if(handler!=null){
            try{
                handler.removeCallbacks(runnable);
            }catch (Exception e){
                Log.e("TrackerServiceERROR", "error inside clearing handler clearAll");
            }

        }

        if(fusedLocationProviderClient != null){
            try{
                fusedLocationProviderClient.removeLocationUpdates(locationCallback);
            }catch (Exception e){
                Log.e("TrackerServiceERROR", "error inside clearing fusedLocationProviderClient clearAll");
            }

        }
    }


    public int getState(){
        return currentState;
    }


    public long getDuration(){
        return duration;
    }


    public double getDistance(){
        return distance;
    }


    public double getPace(){
        return pace;
    }

    private DatabaseHelper getHelper(){
        if(dbHelper == null){
            return OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return dbHelper;
    }
}