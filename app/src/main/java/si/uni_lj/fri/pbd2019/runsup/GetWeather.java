package si.uni_lj.fri.pbd2019.runsup;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class GetWeather extends AsyncTask<Void, Void, String> {

    private static final String stringFormat = "https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&APPID=%s";

    private String lat;
    private String lng;
    private String apiKey;
    private InputStream inputStream = null;

    public GetWeather(String lat, String lng, String apiKey){
        super();
        this.lat = lat;
        this.lng = lng;
        this.apiKey = apiKey;
    }

    @Override
    protected String doInBackground(Void... voids) {

        String URLstring = String.format(stringFormat, lat, lng, apiKey);
        Log.d("weather", URLstring);
        try{
            URL url = new URL(URLstring);
            HttpsURLConnection conn = null;
            conn = (HttpsURLConnection) url.openConnection();

            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();

            if(conn.getResponseCode() == 200){
                inputStream = conn.getInputStream();

                StringBuilder stringBuilder = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String str;
                while((str = reader.readLine()) != null){
                    stringBuilder.append(str);
                }
                reader.close();
                return stringBuilder.toString();
            }

        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if(inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }

        return null;
    }
}
