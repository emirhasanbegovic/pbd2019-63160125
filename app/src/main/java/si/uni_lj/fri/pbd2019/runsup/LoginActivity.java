package si.uni_lj.fri.pbd2019.runsup;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;


import java.sql.SQLException;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.UserProfile;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class LoginActivity extends AppCompatActivity implements
        View.OnClickListener {


    private static final String TAG = "LoginActivity1";
    private static final int RC_SIGN_IN = 9001;
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;

    private TextView userWeight;
    private TextView userAge;
    private TextView userHeight;

    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_login);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        Button signOutButton = findViewById(R.id.sign_out_button);

        TextView textViewWeight = findViewById(R.id.textView3);
        TextView textViewAge = findViewById(R.id.textView5);
        TextView textViewHeight = findViewById(R.id.textView7);

        userWeight = findViewById(R.id.textView4);
        userAge = findViewById(R.id.textView6);
        userHeight = findViewById(R.id.textView8);

        textViewWeight.setOnClickListener(this);
        textViewAge.setOnClickListener(this);
        textViewHeight.setOnClickListener(this);

        signInButton.setOnClickListener(this);
        signOutButton.setOnClickListener(this);

        setupActionBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        userWeight.setText(getUserWeight() + " kg");
    }

    private double getUserWeight(){
        dbHelper = getHelper();
        Dao<User, Long> userDao = null;
        try{
            userDao = dbHelper.userDao();
            List<User> users = userDao.queryForAll();

            if(users.size() > 0){
                User u  = users.get(0);
                List<UserProfile> userProfiles = u.getUserProfiles().getDao().queryBuilder().query();
                if(userProfiles.size()>0){
                    return userProfiles.get(userProfiles.size()-1).getWeight();
                }
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        return 80;
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        updateUI(null);
                        // [END_EXCLUDE]
                    }
                });
        FirebaseAuth.getInstance().signOut();

    }

    private void updateUI(@Nullable FirebaseUser account) {
        if (account != null) {

            String name = account.getDisplayName();
            String userId = account.getUid();
            Log.d(TAG, "waaaaaaaaaaat = " + userId);
            String photo = String.valueOf(account.getPhotoUrl());
            writeToSharedPrefrences(name, 1L, photo);

        } else {
            Log.d(TAG, "account is null");
            removeAllFromPreference();
        }
    }



    private void updateUserData(FirebaseUser account, GoogleSignInAccount acct){
        dbHelper = getHelper();
        Dao<User, Long> userDao = null;
        try{
            userDao = dbHelper.userDao();
            List<User> users = userDao.queryForAll();

            if(users.size() > 0){
                User u  = users.get(0);
                u.setAccId(account.getUid());
                u.setAuthToken(acct.getIdToken());
                userDao.update(u);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void updateUserWeightDatabase(int weight){
        dbHelper = getHelper();
        Dao<User, Long> userDao = null;
        try{
            userDao = dbHelper.userDao();
            List<User> users = userDao.queryForAll();

            if(users.size() > 0) {
                User u = users.get(0);
                UserProfile userProfile = new UserProfile();
                userProfile.setWeight(weight);
                u.getUserProfiles().add(userProfile);
                userDao.update(u);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private DatabaseHelper getHelper(){
        if(dbHelper == null){
            return OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return dbHelper;
    }


    private void writeToSharedPrefrences(String name, Long UID, String photo) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString("userName", name);
        editor.putLong("userId", UID); //TODO get current userId
        editor.putString("photo", photo);
        editor.putBoolean("userSignedIn", true);
        editor.apply();
    }

    private void removeAllFromPreference() {
        PreferenceManager.getDefaultSharedPreferences(this).edit().remove("userName").apply();
        PreferenceManager.getDefaultSharedPreferences(this).edit().remove("userId").apply();
        PreferenceManager.getDefaultSharedPreferences(this).edit().remove("photo").apply();
        PreferenceManager.getDefaultSharedPreferences(this).edit().remove("userSignedIn").apply();
    }


    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        final GoogleSignInAccount googleSignInAccount = acct;
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            updateUI(user);
                            //googleSignInAccount.getIdToken();
                            updateUserData(user, googleSignInAccount);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());

                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                if (!preferences.getBoolean("userSignedIn", false))
                    signIn();
                break;
            case R.id.sign_out_button:
                signOut();
                break;
            case R.id.textView3:
                setDialogBuilder("Weight");
                break;
            case R.id.textView5:
                setDialogBuilder("Age");
                break;
            case R.id.textView7:
                setDialogBuilder("Height");
                break;
        }
    }

    private void setDialogBuilder(String type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(type);

        // Set up the input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);
        final String t = type;
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String userInput = input.getText().toString();

                if (userInput.length() > 0) {
                    switch (t) {
                        case "Weight":
                            userWeight.setText(userInput + " kg");
                            updateUserWeightDatabase(Integer.parseInt(userInput));
                            break;
                        case "Age":
                            userAge.setText(userInput + " years");
                            break;
                        case "Height":
                            userHeight.setText(userInput + "cm");
                            break;
                    }
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

}
