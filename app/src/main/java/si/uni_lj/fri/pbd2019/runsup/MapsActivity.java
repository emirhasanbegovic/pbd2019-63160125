package si.uni_lj.fri.pbd2019.runsup;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private List<LatLng> route;
    private List<Long> sessionNumbersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_maps_map);

        route = new ArrayList<>();
        sessionNumbersList = new ArrayList<>();

        ArrayList<String> arrayList = getIntent().getStringArrayListExtra("locations");
        for(int i = 0; i < arrayList.size(); i++){
            String stringLocation[] = arrayList.get(i).split("###");
            double lat = Double.parseDouble(stringLocation[0]);
            double lon = Double.parseDouble(stringLocation[1]);
            route.add(new LatLng(lat, lon));
            sessionNumbersList.add(Long.parseLong(stringLocation[2]));
        }

        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                displayRoute();
            }
        });
    }

    private void displayRoute(){
        boolean b = false;
        if(route.size()>1) {

            MarkerOptions gpsPointLocation1 = new MarkerOptions().position(route.get(0)).title("Start");
            Log.d("route", "size = "+route.size());
            for (int i = 0; i < route.size() - 1; i++) {


                LatLng latLng1 = route.get(i);
                LatLng latLng2 = route.get(i+1);

                long session1 = sessionNumbersList.get(i);
                long session2 = sessionNumbersList.get(i+1);

                double distanceBetweenPoints = SphericalUtil.computeDistanceBetween(latLng1, latLng2);



                Log.d("route", "risem "+latLng1.toString() + " session = "+session1 + " distanca = "+distanceBetweenPoints);
                Log.d("route", "risem "+latLng2.toString() + " session = "+session2 + " distanca = "+distanceBetweenPoints);

                if(session1!=session2){
                    if(distanceBetweenPoints > 100){
                        MarkerOptions endSession1Point = new MarkerOptions().position(latLng1).title("Pause "+session1);
                        MarkerOptions startSession2Point = new MarkerOptions().position(latLng2).title("Continue "+session2);
                        mMap.addMarker(endSession1Point);
                        mMap.addMarker(startSession2Point);
                        continue;
                    }else{
                        MarkerOptions endPointSession1 = new MarkerOptions().position(latLng1).title("Break "+session1);
                        mMap.addMarker(endPointSession1);
                    }
                }

                Log.d("route", "risem resno");
                mMap.addPolyline(new PolylineOptions()
                        .add(latLng1, latLng2)
                        .width(5)
                        .color(Color.RED));

            }
            MarkerOptions gpsPointLocation2 = new MarkerOptions().position(route.get(route.size()-1)).title("End");
            b = true;
            mMap.addMarker(gpsPointLocation1);
            mMap.addMarker(gpsPointLocation2);
        }else if(route.size()==1){
            MarkerOptions gpsPointLocation = new MarkerOptions().position(route.get(0)).title("Start");
            CameraUpdateFactory.newLatLng(route.get(0));
            mMap.addMarker(gpsPointLocation);
            mMap.animateCamera(CameraUpdateFactory.newLatLng(route.get(0)));
            return;
        }

        if(b){
            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
            for(LatLng latLng : route){
                boundsBuilder.include(latLng);
            }

            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 100));
        }
    }
}
