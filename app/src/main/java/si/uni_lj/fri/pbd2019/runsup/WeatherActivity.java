package si.uni_lj.fri.pbd2019.runsup;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;

public class WeatherActivity extends AppCompatActivity {

    private TextView weatherIconTextView;
    private TextView weatherDescriptionTextView;
    private TextView weatherTemperatureTextView;
    private TextView textViewLoadingText;
    private ProgressBar progressBar;
    private static final int locationRequestCode = 175;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

       setupActionBar();

        weatherIconTextView = findViewById(R.id.weather_icon);
        weatherDescriptionTextView = findViewById(R.id.weather_description);
        weatherTemperatureTextView = findViewById(R.id.temperature_tv);
        textViewLoadingText = findViewById(R.id.weather_loading_text);

        progressBar = findViewById(R.id.weather_progress_bar);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        showPermissionDialog();
        createLocationRequest();
        createLocationCallback();

        Typeface customFont = Typeface.createFromAsset(getAssets(), "fonts/weathericons-regular-webfont.ttf");
        weatherIconTextView.setTypeface(customFont);

        try{
            startLocationUpdates();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showPermissionDialog() {
        if (!checkPermission()) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    locationRequestCode);
        }else{
            startLocationUpdates();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == locationRequestCode){
            startLocationUpdates();
        }else{
            Log.d("weather", "permission denied");
        }
    }

    public boolean checkPermission() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    protected void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void createLocationCallback() {
        locationCallback = new LocationCallback() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                //Toast.makeText(WeatherActivity.this, ""+locationResult.toString(), Toast.LENGTH_SHORT).show();
                Location location = locationResult.getLastLocation();

                new GetWeather(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()), getResources().getString(R.string.open_weather_map_api_key)){
                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                        try {
                            Log.d("weather", s);
                            JSONObject jsonObject = new JSONObject(s);
                            JSONArray jsonArray = jsonObject.getJSONArray("weather");
                            JSONObject weatherElement = (JSONObject) jsonArray.get(0);
                            int id = weatherElement.getInt("id");
                            String description = weatherElement.getString("description");
                            JSONObject main = (JSONObject) jsonObject.get("main");
                            double temp = main.getDouble("temp");
                            updateGUI(id, description, temp);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }.execute();


            }

        };

    }

    private void updateGUI(int id, String description, double temp){
        weatherIconTextView.setVisibility(View.VISIBLE);
        weatherDescriptionTextView.setVisibility(View.VISIBLE);
        weatherTemperatureTextView.setVisibility(View.VISIBLE);
        textViewLoadingText.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);

        id/=100;

        if(id == 2){
            weatherIconTextView.setText(R.string.weather_thunder);
        }else if(id == 8){
            weatherIconTextView.setText(R.string.weather_cloudy);
        }else if(id == 5){
            weatherIconTextView.setText(R.string.weather_rainy);
        }else if(id == 6){
            weatherIconTextView.setText(R.string.weather_snowy);
        }else if(id == 7){
            weatherIconTextView.setText(R.string.weather_foggy);
        }else if(id == 3){
            weatherIconTextView.setText(R.string.weather_drizzle);
        }

        weatherDescriptionTextView.setText(description);
        double celsius = temp-273;
        weatherTemperatureTextView.setText(MainHelper.formatPace(celsius)+" ℃");

    }

    @SuppressLint("MissingPermission")
    protected void startLocationUpdates(){
        try{
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
        }catch (Exception e){
            Log.e("TrackerServiceERROR", "could not create fusedLocationProviderClient"+ e.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return true;
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
