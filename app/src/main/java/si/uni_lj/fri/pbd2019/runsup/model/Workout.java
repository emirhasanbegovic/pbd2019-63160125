package si.uni_lj.fri.pbd2019.runsup.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "Workout")
public class Workout {

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField(foreign = true)
    private User user;

    @DatabaseField(columnName = "title")
    private String title;

    @DatabaseField(columnName = "created")
    private Date created;

    @DatabaseField(columnName = "status")
    private int status;

    @DatabaseField(columnName = "distance")
    private double distance;

    @DatabaseField(columnName = "duration")
    private long duration;

    @DatabaseField(columnName = "totalCalories")
    private double totalCalories;

    @DatabaseField(columnName = "paceAvg")
    private double paceAvg;

    @DatabaseField(columnName = "sportActivity")
    private int sportActivity;

    @DatabaseField(columnName = "lastUpdate")
    private Date lastUpdate;

    @ForeignCollectionField(eager = true)
    ForeignCollection<GpsPoint> gpsPoints;

    public Workout(String title, int sportActivity){
        this.title = title;
        this.sportActivity = sportActivity;
        this.created = new Date();
        this.lastUpdate = new Date();
    }

    public Workout(){
        this.created = new Date();
        this.lastUpdate = new Date();
    }

    // initial status of workout
    public static final int statusUnknown = 0;
    // ended workout
    public static final int statusEnded = 1;
    // paused workout
    public static final int statusPaused = 2;
    // deleted workout
    public static final int statusDeleted = 3;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public double getTotalCalories() {
        return totalCalories;
    }

    public void setTotalCalories(double totalCalories) {
        this.totalCalories = totalCalories;
    }

    public double getPaceAvg() {
        return paceAvg;
    }

    public void setPaceAvg(double paceAvg) {
        this.paceAvg = paceAvg;
    }

    public int getSportActivity() {
        return sportActivity;
    }

    public void setSportActivity(int sportActivity) {
        this.sportActivity = sportActivity;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public ForeignCollection<GpsPoint> getGpsPoints() {
        return gpsPoints;
    }

    public void setGpsPoints(ForeignCollection<GpsPoint> gpsPoints) {
        this.gpsPoints = gpsPoints;
    }
}
