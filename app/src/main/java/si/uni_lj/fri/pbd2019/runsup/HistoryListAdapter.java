package si.uni_lj.fri.pbd2019.runsup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class HistoryListAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> objects;

    public HistoryListAdapter(@NonNull Context context, int resource, ArrayList<String> objects) {

        super(context, resource);
        this.context = context;
        this.objects = objects;
    }


    public void refresh(List<String> list){
        Log.d("updateAdapter", "----------------------------");
        this.objects = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return objects.size();
    }

    @Override
    public String getItem(int position) {
        // TODO Auto-generated method stub
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        String element = objects.get(position);

        String splitString[] = element.split("###");

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.adapter_history, null);

        TextView workoutTitle = view.findViewById(R.id.textview_history_title);
        TextView workoutTime =  view.findViewById(R.id.textview_history_datetime);
        TextView sportActivity =  view.findViewById(R.id.textview_history_sportactivity);


        workoutTitle.setText(splitString[0]);
        workoutTime.setText(splitString[1]);
        sportActivity.setText(splitString[2]);

        final long workoutID = Long.parseLong(splitString[3]);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WorkoutDetailActivity.class);
                intent.putExtra("workoutId", workoutID);
                context.startActivity(intent);
            }
        });

        return view;
    }




}
