package si.uni_lj.fri.pbd2019.runsup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class ShareDialogFragment extends DialogFragment {

    public static ShareDialogFragment newInstance(String title, String workoutType, String map) {
        ShareDialogFragment frag = new ShareDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("workoutType", workoutType);
        args.putString("map", map);
        frag.setArguments(args);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragholder_sharedialog, container);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        EditText editText = view.findViewById(R.id.share_message);
        editText.setText(getArguments().getString("workoutType"));
        if(getArguments().getString("map").equals("NO_MAP")) {
            view.findViewById(R.id.mapFragmentContainer).setVisibility(View.GONE);
        }
    }
    /*
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(title);

        alertDialogBuilder.setView(R.layout.fragholder_sharedialog);


        alertDialogBuilder.setPositiveButton("OK",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success
                Toast.makeText(getContext(), "hahahaha", Toast.LENGTH_SHORT).show();
            }
        });

        alertDialogBuilder.setNegativeButton("NO",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success
                dialog.dismiss();
            }
        });



        return alertDialogBuilder.create();
    }*/


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Fragment f = getFragmentManager().findFragmentById(R.id.mapFragmentContainer);
        if(f!=null){
            getFragmentManager().beginTransaction().remove(f).commit();
        }
    }


}
