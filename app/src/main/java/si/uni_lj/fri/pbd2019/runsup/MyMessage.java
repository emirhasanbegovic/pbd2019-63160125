package si.uni_lj.fri.pbd2019.runsup;

public class MyMessage {

    private String name;
    private String content;
    private String time;
    private String userImgURL;
    private String type;

    public MyMessage(String name, String content, String time, String userImgURL, String type){
        this.name = name;
        this.content = content;
        this.time = time;
        this.userImgURL = userImgURL;
        this.type = type;

    }

    public String getName(){
        return name;
    }

    public String getContent(){
        return content;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public String getUserImgURL() {
        return userImgURL;
    }

    public void setUserImgURL(String userImgURL) {
        this.userImgURL = userImgURL;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString(){
        return name + ";"+content;
    }

}
