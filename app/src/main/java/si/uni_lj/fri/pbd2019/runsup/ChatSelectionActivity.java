package si.uni_lj.fri.pbd2019.runsup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import si.uni_lj.fri.pbd2019.runsup.fragments.ChatFragment;

public class ChatSelectionActivity extends AppCompatActivity {

    private TextView textViewRunner;
    private TextView textViewWalkers;
    private TextView textViewCyclers;
    private SharedPreferences sharedPreferences;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_select);
        setupActionBar();
        sharedPreferences =  PreferenceManager.getDefaultSharedPreferences(this);
        Intent notifyIntent = getIntent();

        textViewRunner = findViewById(R.id.running_tv_chat);
        textViewWalkers = findViewById(R.id.walking_tv_chat);
        textViewCyclers = findViewById(R.id.cyclcing_tv_chat);


        boolean isChatFragment = notifyIntent.getBooleanExtra("isChatFragment", false);
        if(isChatFragment){
            String chat = notifyIntent.getStringExtra("chat");
            String user = notifyIntent.getStringExtra("user");
            startFragment(chat, user);
            return;
        }



        textViewRunner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFragment("runners","");
            }
        });

        textViewWalkers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFragment("walkers","");
            }
        });

        textViewCyclers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFragment("cyclers","");
            }
        });

    }

    private boolean userLoggedIn(){
        return sharedPreferences.getBoolean("userSignedIn", false);
    }

    private void startFragment(String groupName, String userSender){
        if(!userLoggedIn()){
            Toast.makeText(this, "In order to join chat you need to login with your google account.", Toast.LENGTH_SHORT).show();
            return;
        }
        textViewRunner.setVisibility(View.GONE);
        textViewWalkers.setVisibility(View.GONE);
        textViewCyclers.setVisibility(View.GONE);

        ChatFragment fragment = ChatFragment.newInstance();
        Bundle args = new Bundle();
        args.putString("groupName", groupName);
        args.putString("userSender", userSender);
        fragment.setArguments(args);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.chat_fragment_holder, fragment).commit();
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return true;
    }
}
