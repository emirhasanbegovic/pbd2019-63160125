package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class ServerSync {

    private static ServerSync instance = null;
    private static FirebaseAuth mAuth;
    private static Context context;
    private static String TAG = "ServerSync1";
    private static DatabaseHelper dbHelper = null;
    private static FirebaseFirestore db;


    public static ServerSync getServerSyncInstance(Context ctx){

        if(instance!=null){
            return instance;
        }

        mAuth = FirebaseAuth.getInstance();
        context = ctx;
        instance = new ServerSync();
        dbHelper = getHelper();
        db = FirebaseFirestore.getInstance();
        return instance;

    }

    public static boolean isLoggedIn(){
        return mAuth.getCurrentUser()!=null;
    }

    public void syncUserData(final SwipeRefreshLayout swipeRefreshLayout, final HistoryFragment historyFragment){
        if(isLoggedIn()){
            db.collection("Workouts").document(mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){
                        DocumentSnapshot d = task.getResult();

                        if(d != null && d.exists()){

                            Map<String, Object> hm = new HashMap<>();
                            hm = d.getData();

                            if(hm!=null){
                                for(String key : hm.keySet()){
                                    HashMap<String, Object> hmW = (HashMap<String, Object>)hm.get(key);
                                    Timestamp t = (Timestamp) hmW.get("created");
                                    Date dateFromDb = t.toDate();
                                    if(getWorkout(dateFromDb)==null){
                                        Log.d(TAG, "NAŠU SM WORKOUT KI JE V BAZI ODDALJENI NI PA MOJI, ZATO MORAM DODAT V LOKALNO");
                                        addToLocalDatabase(hmW);
                                    }
                                }
                                Log.d(TAG, hm.toString());
                            }else{
                                Log.d(TAG, "HADUKEN "+hm.toString());
                            }

                        }
                        removeAllFromFirebaseAndPush(historyFragment);

                        if(swipeRefreshLayout!=null){
                            swipeRefreshLayout.setRefreshing(false);
                        }

                    }else{
                        Log.d("ServerSync1", "fuck");
                    }
                }
            });
        }

    }

    private static int longToInt(long l){
        return Long.valueOf(l).intValue();
    }

    private static void addToLocalDatabase(HashMap<String, Object> fireBaseWorkout){
        Workout workout = new Workout();
        workout.setStatus(longToInt((long)fireBaseWorkout.get("status")));
        workout.setTitle((String)fireBaseWorkout.get("title"));

        Timestamp timestamp1 = (Timestamp)fireBaseWorkout.get("created");
        workout.setCreated(timestamp1.toDate());
        Log.d(TAG, "v metodi AddToLocalDatabase sm lihkar povozu created in mo dou na" + workout.getCreated().toString());
        workout.setDistance((double)fireBaseWorkout.get("distance"));
        workout.setDuration((long)fireBaseWorkout.get("duration"));
        workout.setTotalCalories((double)fireBaseWorkout.get("totalCalories"));
        workout.setPaceAvg((double)fireBaseWorkout.get("paceAvg"));
        workout.setSportActivity(longToInt((long)fireBaseWorkout.get("sportActivity")));

        Timestamp timestamp2 = (Timestamp)fireBaseWorkout.get("lastUpdate");
        workout.setLastUpdate(timestamp2.toDate());


        HashMap<String, HashMap<String,Object>> workoutGpsPoints =  (HashMap<String, HashMap<String,Object>>) fireBaseWorkout.get("gpsPoints");

        for(String gpsPointKey : workoutGpsPoints.keySet()){
            HashMap h = workoutGpsPoints.get(gpsPointKey);

            /*
                 public GpsPoint(Workout workout, long sessionNumber, Location location, long duration, float speed, double pace, double totalCalories){
                    this.workout = workout;
                    this.sessionNumber = sessionNumber;
                    this.latitude = location.getLatitude();
                    this.longitude = location.getLongitude();
                    this.duration = duration;
                    this.speed = speed;
                    this.pace = pace;
                    this.totalCalories = totalCalories;
            }

             */

            GpsPoint p = new GpsPoint();
            p.setSessionNumber(longToInt((long)h.get("sessionNumber")));
            p.setLatitude((double)h.get("latitude"));
            p.setLongitude((double)h.get("longitude"));
            p.setDuration((long)h.get("duration"));

            double speedDouble = (double)h.get("speed");
            float speedFloat = (float)speedDouble;

            p.setSpeed(speedFloat);
            p.setPace((double)h.get("pace"));
            p.setTotalCalories((double)h.get("totalCalories"));
            p.setCreated(((Timestamp)h.get("created")).toDate());
            p.setLastUpdate(((Timestamp)h.get("lastUpdate")).toDate());

            p.setWorkout(workout);
            addPointToDatabase(p);
        }

        addWorkoutToDatabase(workout);

    }

    private static void addPointToDatabase(GpsPoint gpsPoint){
        dbHelper = getHelper();
        Dao<GpsPoint, Long> gpsDao = null;
        try{
            gpsDao = dbHelper.gpsPointDao();
            gpsDao.create(gpsPoint);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private static void addWorkoutToDatabase(Workout workout){
        dbHelper = getHelper();
        Dao<Workout, Long> workoutDao = null;
        try {
            workoutDao = dbHelper.workoutDao();
            Log.d(TAG, "v metodi addWorkoutToDatabase pa sm lihkar dau not "+workout.getCreated());
            workoutDao.create(workout);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private static void removeAllFromFirebaseAndPush(final HistoryFragment historyFragment){
        db.collection("Workouts").document(mAuth.getCurrentUser().getUid()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "brisanje uspesno, lah use pusham!");
                    pushToDatabase(historyFragment);
                }else{
                    Log.d(TAG, "brisanje neuspesno");
                }
            }
        });
    }

    private static void pushToDatabase(final HistoryFragment historyFragment){
        List<Workout> workouts = getWorkouts();
        HashMap<String, Object> hashMapMain = new HashMap<>();
        if(workouts!=null){
            Log.d(TAG, "WORKOUTOV MAM "+workouts.size());
            for(int i = 0; i < workouts.size(); i++){
                Workout w = workouts.get(i);
                Log.d(TAG, "status MAM "+w.getStatus());
                if(w.getStatus()==Workout.statusDeleted || w.getStatus()==Workout.statusEnded){
                    HashMap<String, Object> currentWorkout = new HashMap<>();

                    currentWorkout.put("title", w.getTitle());
                    currentWorkout.put("duration", w.getDuration());
                    currentWorkout.put("totalCalories", w.getTotalCalories());
                    currentWorkout.put("status", w.getStatus());
                    currentWorkout.put("distance", w.getDistance());
                    currentWorkout.put("paceAvg", w.getPaceAvg());
                    currentWorkout.put("sportActivity", w.getSportActivity());
                    currentWorkout.put("lastUpdate", w.getLastUpdate());
                    currentWorkout.put("created", w.getCreated());

                    HashMap<String, Object> hashMapPoints = new HashMap<>();
                    int j = 0;
                    for(GpsPoint gpsPoint : workouts.get(i).getGpsPoints()){
                        HashMap<String, Object> hashMapPoint = new HashMap<>();
                        hashMapPoint.put("created", gpsPoint.getCreated());
                        hashMapPoint.put("duration", gpsPoint.getDuration());
                        hashMapPoint.put("lastUpdate", gpsPoint.getLastUpdate());
                        hashMapPoint.put("latitude", gpsPoint.getLatitude());
                        hashMapPoint.put("longitude", gpsPoint.getLongitude());
                        hashMapPoint.put("totalCalories", gpsPoint.getTotalCalories());
                        hashMapPoint.put("pace", gpsPoint.getPace());
                        hashMapPoint.put("speed", gpsPoint.getSpeed());
                        hashMapPoint.put("sessionNumber", gpsPoint.getSessionNumber());
                        j++;
                        hashMapPoints.put(""+j, hashMapPoint);
                    }
                    currentWorkout.put("gpsPoints", hashMapPoints);
                    hashMapMain.put(""+i, currentWorkout);
                }
            }



            Log.d(TAG, "ALO" +hashMapMain.toString());
            db.collection("Workouts").document(mAuth.getCurrentUser().getUid()).set(hashMapMain).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Log.d(TAG, "uspesno dodan");

                        updateArrayAdapter(historyFragment);
                    }else{
                        Log.d(TAG, "neuspesno dodan");
                    }
                }
            });

        }
    }

    private static void updateArrayAdapter(final HistoryFragment historyFragment){
        //workoutList.clear();
        if(historyFragment!=null){
            historyFragment.updateAdapter();
        }
    }



    private static Workout getWorkout(Date date){
        dbHelper = getHelper();
        Dao<Workout, Long> workoutDao = null;
        try{
            workoutDao = dbHelper.workoutDao();
            List<Workout> workouts = workoutDao.queryBuilder().where().eq("created", date).query();
            if(workouts.size()>0){
                return workouts.get(0);
            }
            return null;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    private static List<Workout> getWorkouts(){
        dbHelper = getHelper();
        Dao<Workout, Long> workoutDao = null;
        try{
            workoutDao = dbHelper.workoutDao();
            return workoutDao.queryForAll();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    private static DatabaseHelper getHelper(){
        if(dbHelper == null){
            return OpenHelperManager.getHelper(context, DatabaseHelper.class);
        }
        return dbHelper;
    }

    public void setSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout, HistoryFragment historyFragment){
        syncUserData(swipeRefreshLayout, historyFragment);
        //swipeRefreshLayout.setRefreshing(false);
    }

}
