package si.uni_lj.fri.pbd2019.runsup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;


public class ActiveWorkoutMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    public class LocationReceiver extends BroadcastReceiver {

        private int updateRouteTimer;
        private ArrayList<LatLng> route;
        private boolean firstTimeMapRender;
        private ArrayList<Polyline> polylines;

        public LocationReceiver(ArrayList<LatLng> route){
            this.route = route;
            updateRouteTimer = 0;
            firstTimeMapRender = true;
            polylines = new ArrayList<>();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            double lat = intent.getDoubleExtra("currentLocationLat", 0.0);
            double lon = intent.getDoubleExtra("currentLocationLon", 0.0);
            updateRouteTimer++;

            if(updateRouteTimer == 15 || firstTimeMapRender){

               polylines.clear();

                for(int i = 0; i < route.size()-1; i++){
                    Polyline line = mMap.addPolyline(new PolylineOptions()
                            .add(route.get(i), route.get(i+1))
                            .width(5)
                            .color(Color.RED));
                    polylines.add(line);
                }

                firstTimeMapRender = false;

                updateRouteTimer=0;
            }
            if(lat!=0){
                if(mMap!=null){
                    LatLng novaLokacija = new LatLng(lat,lon);
                    route.add(novaLokacija);
                    currentMarkerOG.remove();
                    currentMarkerOG = mMap.addMarker(new MarkerOptions().position(novaLokacija));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(novaLokacija, 18));
                }
            }
        }
    }

    private GoogleMap mMap;
    private Button backButton;

    private static final String TAG = "ActiveWorkoutMapActivity1";
    private LocationReceiver locationReceiver;

    private double stopWatchLat;
    private double stopWatchLon;

    private LatLng currentLatLing;
    private MarkerOptions currentMarker;
    private Marker currentMarkerOG;
    private ArrayList<LatLng> currentRoute;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_workout_map);

        backButton = findViewById(R.id.button_activeworkoutmap_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActiveWorkoutMapActivity.this.finish();
            }
        });

        currentRoute = (ArrayList<LatLng>) getIntent().getSerializableExtra("locs");
        stopWatchLat = getIntent().getDoubleExtra("initLat", 0);
        stopWatchLon = getIntent().getDoubleExtra("initLon", 0);
        currentLatLing = new LatLng(stopWatchLat, stopWatchLon);
        currentMarker = new MarkerOptions().position(currentLatLing);

        locationReceiverRegistration();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_activeworkoutmap_map);

        locationReceiverRegistration();
        mapFragment.getMapAsync(this);
    }

    private void locationReceiverRegistration(){
        if(locationReceiver==null){
            IntentFilter filter = new IntentFilter();
            filter.addAction(PossibleActions.TICK);
            locationReceiver = new LocationReceiver(currentRoute);
            registerReceiver(locationReceiver, filter);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        currentMarkerOG = mMap.addMarker(currentMarker);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLing, 14));
    }
}
