package si.uni_lj.fri.pbd2019.runsup.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "User")
public class User {

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField(columnName = "accId")
    private String accId;

    @DatabaseField(columnName = "authToken")
    private String authToken;

    @ForeignCollectionField(eager = true)
    ForeignCollection<UserProfile> userProfiles;

    @ForeignCollectionField(eager = true)
    ForeignCollection<Workout> workouts;

    public User(){

    }

    public User(String accId){
        this.accId = accId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public ForeignCollection<UserProfile> getUserProfiles() {
        return userProfiles;
    }

    public void setUserProfiles(ForeignCollection<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
    }

    public ForeignCollection<Workout> getWorkouts() {
        return workouts;
    }

    public void setWorkouts(ForeignCollection<Workout> workouts) {
        this.workouts = workouts;
    }

}
