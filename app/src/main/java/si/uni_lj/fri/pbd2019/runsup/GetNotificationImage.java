package si.uni_lj.fri.pbd2019.runsup;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetNotificationImage extends AsyncTask<Void, Void, Bitmap> {

    InputStream inputStream = null;
    String imageURL;

    public GetNotificationImage(String imageURL){
        super();
        this.imageURL = imageURL;
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        try{
            URL url = new URL(imageURL);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            inputStream = connection.getInputStream();
            return BitmapFactory.decodeStream(inputStream);
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }finally {
            if(inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
