package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import si.uni_lj.fri.pbd2019.runsup.ChatSelectionActivity;
import si.uni_lj.fri.pbd2019.runsup.GetNotificationImage;
import si.uni_lj.fri.pbd2019.runsup.MyMessage;
import si.uni_lj.fri.pbd2019.runsup.MessageAdapter;
import si.uni_lj.fri.pbd2019.runsup.R;

public class ChatFragment extends Fragment {

    private ArrayAdapter<MyMessage> messageArrayAdapter;
    private ListView listView;
    private ArrayList<MyMessage> messageList = new ArrayList<>();
    private Socket socket;
    private SharedPreferences preferences;
    private EditText userInput;
    private ImageButton sendButton;
    private ImageButton uploadImageButton;
    private static final int SELECT_IMAGE = 69;
    private ProgressBar progressBarChatLoading;
    private LinearLayout linearLayoutInputHolder;
    private TextView someoneIsTypingTV;



    public static ChatFragment newInstance() {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    private String getCurrentChat() {
        Bundle args = getArguments();
        return args.getString("groupName");
    }

    private String getSubedUser(){
        Bundle args = getArguments();
        return args.getString("userSender");
    }


    private int findCorrectIndex(){
        String subUser = getSubedUser();

        if(subUser.length()==0){
            return -1;
        }

        int indexToReturn = 0;

        for(int i = 0; i < messageList.size(); i++){
            if(messageList.get(i).getName().equals(subUser)){
                indexToReturn = i;
            }
        }

        return indexToReturn;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        socketInit();
    }

    public boolean isSubscribedTo(String otherUser) {
        Log.d("newmessage", "isSubscribedTo");
        String userSubscriptions = preferences.getString("subscriptions", "");
        String arr[] = userSubscriptions.split(";");

        for (int i = 0; i < arr.length; i++) {
            Log.d("newmessage", "element = isSubscribedTo " + arr[i]);
            if (arr[i].equals(otherUser)) {
                return true;
            }
        }

        return false;
    }

    public String getUsername() {
        return preferences.getString("userName", "Unknown");
    }


    public void socketInit() {

        try {
            socket = IO.socket("http://51.15.238.210:2000/");
            getAllMessages();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("userName", getUsername());
                    jsonObject.put("groupName", getCurrentChat());
                    socket.emit("join", jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }).on("newmessage", new Emitter.Listener() {

            @Override
            public void call(Object... args) {

                try {
                    final JSONObject object = new JSONObject(args[0].toString());
                    Log.d("newmessage", object.getString("userName"));
                    Log.d("newmessage", object.getString("content"));

                    boolean subscriptionBoolean = isSubscribedTo(object.getString("userName"));
                    boolean userIsActiveInThisChat = object.getString("groupName").equals(getCurrentChat());

                    Log.d("newmessage", subscriptionBoolean + " , " + userIsActiveInThisChat);

                    if (!subscriptionBoolean && !userIsActiveInThisChat) {
                        Log.d("newmessage", "sporocilo ni bilo meni namenjeno!");
                        return;
                    }

                    if (subscriptionBoolean && !userIsActiveInThisChat) {
                        showNotification(object.getString("userName"),
                                object.getString("content"),
                                object.getString("img"),
                                object.getString("type"),
                                object.getString("groupName"));
                        return;
                    }

                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    messageList.add(new MyMessage(
                                            object.getString("userName"),
                                            object.getString("content"),
                                            object.getString("time"),
                                            object.getString("img"),
                                            object.getString("type")));
                                    Log.d("newmessage", object.getString("content"));
                                    Log.d("newmessage", messageList.toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                listView.setSelection(messageArrayAdapter.getCount() - 1);
                            }
                        });
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }).on("newUserJoined", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    final JSONObject object = new JSONObject(args[0].toString());
                    Log.d("newUserJoined", object.getString("userName"));
                    if (!object.getString("userName").equals(getUsername())) {
                        return;
                    }
                    Log.d("newUserJoined", object.toString());

                    JSONArray loadedMessages = object.getJSONArray("data");
                    messageList.clear();
                    for (int i = 0; i < loadedMessages.length(); i++) {
                        JSONObject jsonObjectElement = (JSONObject) loadedMessages.get(i);

                        String userName = jsonObjectElement.getString("userName");
                        String content = jsonObjectElement.getString("content");
                        String time = jsonObjectElement.getString("time");
                        String imgURL = jsonObjectElement.getString("img");
                        String msgType = jsonObjectElement.getString("type");

                        messageList.add(new MyMessage(userName, content, time, imgURL, msgType));
                    }
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    progressBarChatLoading.setVisibility(View.GONE);
                                    userInput.setVisibility(View.VISIBLE);
                                    sendButton.setVisibility(View.VISIBLE);
                                    uploadImageButton.setVisibility(View.VISIBLE);
                                    linearLayoutInputHolder.setVisibility(View.VISIBLE);
                                    listView.setVisibility(View.VISIBLE);

                                    if (object.getString("userName").equals(getUsername())) {
                                     //   Toast.makeText(getActivity(), "Welcome back " + object.getString("userName"), Toast.LENGTH_SHORT).show();
                                    } else {
                                     //   Toast.makeText(getActivity(), "User " + object.getString("userName") + "has joined the chat!", Toast.LENGTH_SHORT).show();
                                    }

                                    messageArrayAdapter.notifyDataSetChanged();
                                    int chIndex = findCorrectIndex();
                                    if(chIndex == -1){
                                        listView.setSelection(messageArrayAdapter.getCount() - 1);
                                    }else{
                                        listView.setSelection(chIndex);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("haduken", args[0].toString());
            }
        }).on("someoneistyping", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.d("stoptyping", "someoneistyping "+args[0].toString() + " ==== "+getCurrentChat());
                if(args[0].toString().equals(getCurrentChat())){

                    if(getActivity() != null){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                someoneIsTypingTV.setVisibility(View.VISIBLE);
                            }
                        });
                    }


                }
            }
        }).on("stoptyping", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("stoptyping", "vsi nehal tipkat "+args[0].toString() + " ==== "+getCurrentChat());
                if(args[0].toString().equals(getCurrentChat())){

                    if(getActivity() != null){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                someoneIsTypingTV.setVisibility(View.GONE);
                            }
                        });
                    }

                }
            }
        });

        socket.connect();

    }



    @SuppressLint("StaticFieldLeak")
    private void showNotification(final String otherUser, final String msg, String imageURL, final String msgType, final String changeChat) {
        final NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "RunsUp Subscription";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
            notificationChannel.setDescription("Sample Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }

        }
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getActivity(), NOTIFICATION_CHANNEL_ID);


        new GetNotificationImage(imageURL) {
            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                Log.d("dobilsemsliko", bitmap.toString());
                notificationBuilder.setLargeIcon(bitmap);
                Intent intent = new Intent(getActivity(), ChatSelectionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("chat", changeChat);
                intent.putExtra("user", otherUser);
                intent.putExtra("isChatFragment", true);
                PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(),999, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                if(msgType.equals("text")){
                    notificationBuilder.setAutoCancel(true)
                            .setDefaults(Notification.DEFAULT_ALL)
                            .setWhen(System.currentTimeMillis())
                            .setTicker("RunsUp")
                            .setContentTitle("New message by " + otherUser)
                            .setContentInfo("Information")
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(pendingIntent)
                            .setLargeIcon(bitmap)
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .bigText(msg)
                            ).build();
                }else{
                    notificationBuilder.setAutoCancel(true)
                            .setDefaults(Notification.DEFAULT_ALL)
                            .setWhen(System.currentTimeMillis())
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("New message by " + otherUser)
                            .setContentText("Uploaded img")
                            .setContentIntent(pendingIntent)
                            .setContentInfo("Information");
                }


                if (msgType.equals("text")) {
                    notificationBuilder.setContentText(msg);
                } else {
                    notificationBuilder.setContentText("User uploaded img");
                }

                if (notificationManager != null) {
                    Log.d("fuckmylife", "hah222222!");
                    notificationManager.notify(999, notificationBuilder.build());
                }
            }
        }.execute();

    }


    public void getAllMessages() {
        JSONObject jsonObject = new JSONObject();
        Log.d("getAllMessages", "emitam !!!!");
        try {
            jsonObject.put("userName", getUsername());
            jsonObject.put("groupName", getCurrentChat());
            socket.emit("imback", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getUserImageURL() {
        return preferences.getString("photo", "none");
    }

    public View.OnClickListener sendButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String userInputString = userInput.getText().toString();

            if (userInputString.length() > 0) {
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                userInput.setText("");
                sendMessage("text", userInputString);
            }
        }
    };

    private void sendMessage(String type, String userInputString) {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String formattedDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy").format(date);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("userName", getUsername());
            jsonObject.put("groupName", getCurrentChat());
            jsonObject.put("content", userInputString);
            jsonObject.put("time", formattedDate);
            jsonObject.put("img", getUserImageURL());
            jsonObject.put("type", type);
            socket.emit("newMessage", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public View.OnClickListener sendImageButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (socket == null) {
                return;
            }
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();

                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bos);
                        byte[] bArray = bos.toByteArray();
                        String encoded = Base64.encodeToString(bArray, Base64.DEFAULT);
                        sendMessage("imageMessage", encoded);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "CANCELED IMAGE UPLOAD", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sendEmptyMessage();
        socket.disconnect();
    }

    private void sendEmptyMessage(){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("userName", getUsername());
            jsonObject.put("groupName", getCurrentChat());
            jsonObject.put("content", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        socket.emit("newMessage", jsonObject.toString());
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            Log.d("tipka", "user trenutno tipka");


            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("userName", getUsername());
                jsonObject.put("groupName", getCurrentChat());
                jsonObject.put("content", "");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            if(s.length()==0){
                socket.emit("newMessage", jsonObject.toString());
                return;
            }else {
                socket.emit("typing", jsonObject.toString());
            }


        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = view.findViewById(R.id.chat_view);
        userInput = view.findViewById(R.id.input_message_et);
        someoneIsTypingTV = view.findViewById(R.id.someone_is_typing_tv);
        userInput.addTextChangedListener(textWatcher);

        progressBarChatLoading = view.findViewById(R.id.chat_loading_bar);
        linearLayoutInputHolder = view.findViewById(R.id.full_user_input_layout_holder);
        sendButton = view.findViewById(R.id.button_send);
        uploadImageButton = view.findViewById(R.id.button_send_image);

        sendButton.setOnClickListener(sendButtonListener);
        uploadImageButton.setOnClickListener(sendImageButtonListener);

        messageArrayAdapter = new MessageAdapter(getActivity(), 0, messageList, getUsername());
        listView.setAdapter(messageArrayAdapter);
    }

}
