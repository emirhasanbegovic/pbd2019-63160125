# RunsUp, Android app
## Author: Emir Hasanbegović
## Contact info: eh7718@student.uni-lj.si

### Project description
The goal of this project is to create an android application that will serve as a personal fitness assistant. Based on the users location it will provide a detailed description of his workout (route, pace, duration, burned calories, ...). In case that the user logs in with his gmail account he can store finished workouts on the server, so if user changes his phone he can always access his previous workout information. The benefit of being logged in with a gmail account is the chat functionality which allows the user to send text messages and images from his image gallery to other users.

### List of libraries (added in sprint #4):
- Client side (Android) socket.io-client
- Server side (Node.js) express, mongodb, socket.io

### Sprint 4 extension
- I've added a chat functionality where users get divided into three groups depending on activities defined in previous sprints (walkers, cyclers, runners). Users can share their workout experience by sending text messages or by uploading photos from the image gallery. In order to get notified of a user's post in chat you can subscribe to him by clicking on his profile picture shown every time a message is sent (you can also unsubsribe if you want to stop getting notified). When  a user clicks on the notification he is navigated to the message located at the bottom (note: notification pops up only when you aren't active in the same group as the user you're subscribed to). Users can also see when someone is typing in their group (the text 'Somone is typing' is shown above the input field). To use the chat functionality you must be logged in with your google account.

- App also contains a seperate activity where users can get current weather information depending on their geolocation.

- Chat server is implemented with Node.js (hosted by Scaleway) and uses mongodb (hosted by mLab) for storing messages.


# 1) Chat group selection 

![chat_selection](https://i.imgur.com/Vg4cwAu.png)

# 2) Basic chat

![basic_chat](https://i.imgur.com/MqUhCQT.png)

# 3) Indicator that someone is typing

![basic_chat](https://i.imgur.com/Edqv2Qq.png)

# 4) Notifications

![basic_chat](https://i.imgur.com/EUOfroO.png)

# 5) Weather information

![weather](https://i.imgur.com/0UsnbHU.png)




